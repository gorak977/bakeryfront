import { Customer } from './customer.interface';

export interface ProductsOrderIncome {
    idOrder: number;
    time: Date;
    totalPrice: number;
    netIncome: number;
    idOrderStatus: number;
    status: string;
    idCustomer: number;
    customer: Customer;
}
