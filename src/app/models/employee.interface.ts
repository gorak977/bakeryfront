import { Address } from './address.interface';
import { Position } from './position.interface';

export interface Employee {
    idEmployee: number;
    name: string;
    surname: string;
    phoneNumber: string;
    experience: string;
    idAddress: number;
    address: Address;
    idPosition: number;
    position: Position;
}
