import { RawStaff } from './rawStaff.interface';
import { Employee } from './employee.interface';

export interface RemovedRawStaff {
    idRemovedRawStaff: number;
    reasonToRemove: string;
    removingDate: Date;
    idEmployee: number;
    amount: number;
    idRawStaff: number;
    employee: Employee;
    rawStaff: RawStaff;
}
