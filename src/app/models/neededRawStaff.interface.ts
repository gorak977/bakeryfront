export interface NeededRawStaff {
     neededAmount: number;
     amountOnStorage: number;
     quantityToManufacture: number;
     idRawStaff: number;
     rawStaffName: string;
}
