export interface Ingredient {
    idIngredient: number;
    weight: number;
    idMeasure: number;
    measureName: string;
    idProduct: number;
    productName: string;
    idRawStaff: number;
    rawStaffName: string;
}
