import { ProductsOrderIncome } from './productsOrderIncome.interface';

export interface Incomes {
  fullIncomes: number;
  netIncome: number;
  month: string;
  orders: ProductsOrderIncome[];
}
