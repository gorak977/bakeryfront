export interface Address {
    idAddress: number;
    country: string;
    city: string;
    street: string;
    houseNumber: number;
}
