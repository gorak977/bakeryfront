export interface User {
    userId: number;
    userName: string;
    password: string;
    role: number;
}

export interface UserRole {
    roleIndex: number;
    roleName: string;
}
