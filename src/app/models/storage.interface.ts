import { Address } from './address.interface';

export interface Storage {
    idStorage: number;
    idAddress: number;
    address: Address;
}
