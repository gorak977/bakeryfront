import { Address } from './address.interface';

export interface Customer {
    idCustomer: number;
    name: string;
    phoneNumber: string;
    idAddress: number;
    address: Address;
}
