import { Product } from './product.interface';
import { Address } from './address.interface';

export interface ProductOnStorage {
    idProductOnStorage: number;
    idProduct: number;
    product: Product;
    idStorage: number;
    storageAddress: Address;
    amount: number;
    dateOfManufacture: Date;
}
