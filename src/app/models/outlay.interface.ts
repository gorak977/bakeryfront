import { RawStaffOrder } from './rawStaffOrder.interface';
import { Employee } from './employee.interface';

export interface Outlay{
    totalOutlay: number;
    totalOutlayWithSalary: number;
    month: string;
    orders: RawStaffOrder[];
    employees: Employee[];
}