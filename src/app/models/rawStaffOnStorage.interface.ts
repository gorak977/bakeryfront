import { RawStaff } from './rawStaff.interface';
import { Address } from './address.interface';

export interface RawStaffOnStorage {
    idRawStaffOnStorage: number;
    idRawStaff: number;
    rawStaff: RawStaff;
    idStorage: number;
    storageAddress: Address;
    amount: number;
    consumeUntil: Date;
}
