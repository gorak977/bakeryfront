import { Customer } from './customer.interface';

export interface ProductsOrder {
    idOrder: number;
    time: Date;
    totalPrice: number;
    idOrderStatus: number;
    status: string;
    idCustomer: number;
    customer: Customer;
}
