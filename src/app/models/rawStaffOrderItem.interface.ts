import { RawStaff } from './rawStaff.interface';
import { RawStaffOrder } from './rawStaffOrder.interface';

export interface RawStaffOrderItem {
    idRawStaffOrderItem: number;
    idRawStaff: number;
    rawStaff: RawStaff;
    amount: number;
    idOrder: number;
    order: RawStaffOrder;
}
