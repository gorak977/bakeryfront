import { Product } from './product.interface';
import { Employee } from './employee.interface';

export interface RemovedProduct {
    idRemovedProduct: number;
    reasonToRemove: string;
    removingDate: Date;
    idEmployee: number;
    amount: number;
    idProduct: number;
    employee: Employee;
    product: Product;
}
