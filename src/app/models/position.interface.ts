export interface Position{
    idPosition: number;
    positionName: string;
    salary: number;
}