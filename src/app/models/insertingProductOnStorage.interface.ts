import { ProductOnStorage } from './productOnStorage.interface';

export interface IncertingProductOnStorage {
    product: ProductOnStorage;
    employeesId: number[];
}
