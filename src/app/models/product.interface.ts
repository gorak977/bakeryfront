export interface Product {
    idProduct: number;
    productName: string;
    weight: number;
    desctiption: string;
    expirationTerm: number;
    price: number;
    markUp: number;
    idMeasure: number;
    measureName: string;
}
