import { Supplier } from './supplier.interface';

export interface RawStaffOrder {
    idOrder: number;
    time: Date;
    totalPrice: number;
    idOrderStatus: number;
    status: string;
    idSupplier: number;
    supplier: Supplier;
}
