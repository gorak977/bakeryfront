import { Address } from './address.interface';

export interface Supplier {
    idSupplier: number;
    name: string;
    phoneNumber: string;
    idAddress: number;
    address: Address;
}
