export interface RawStaff {
    idRawStaff: number;
    name: string;
    expirationDate: number;
    pricePerMeasure: number;
    idMeasure: number;
    measureName: string;
}
