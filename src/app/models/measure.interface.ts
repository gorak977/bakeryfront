export interface Measure {
    idMeasure: number;
    name: string;
    partOfKg: number;
}
