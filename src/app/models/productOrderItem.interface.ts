import { Product } from './product.interface';
import { ProductsOrder } from './productsOrder.interface';

export interface ProductOrderItem {
    idProductOrderItem: number;
    idProduct: number;
    product: Product;
    amount: number;
    idOrder: number;
    order: ProductsOrder;
}
