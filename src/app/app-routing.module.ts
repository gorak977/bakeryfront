

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './components/products-components/products/products.component';
import { CustomersComponent } from './components/customers-components/customers-component/customers.component';
import { MeasureComponent } from './components/measure-components/measure/measure.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ProductOnStorageComponent } from './components/product-on-storage-components/product-on-storage/product-on-storage.component';
import { SuppliersComponent } from './components/suppliers-components/suppliers-component/suppliers.component';
import { EmployeesComponent } from './components/employees-components/employees/employees.component';
import { RawStaffComponent } from './components/raw-staff-components/raw-staff/raw-staff.component';
import { RemovedProductsListComponent } from './components/product-on-storage-components/removed-products-list/removed-products-list.component';
import { ProductOrdersComponent } from './components/product-orders-components/product-orders/product-orders.component';
import { RawStaffOrdersComponent } from "./components/raw-staff-order-components/raw-staff-orders/raw-staff-orders.component";
import { RawStaffOnStorageComponent } from './components/raw-staff-on-storage-component/raw-staff-on-storage/raw-staff-on-storage.component';
import { RemovedRawStaffListComponent } from './components/raw-staff-on-storage-component/removed-raw-staff-list/removed-raw-staff-list.component';
import { OutlayComponent } from './components/financial-accounting-components/outlay-component/outlay.component';
import { IncomesComponent } from './components/financial-accounting-components/incomes-component/incomes.component';
import { LogInComponent } from './components/auth/log-in/log-in.component';
import { AuthGuard } from './guards/auth.guard';
import { RegisterComponent } from './components/auth/register/register.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    canActivate: [AuthGuard],
    component: LogInComponent,
  },
  {
    path: 'register',
    canActivate: [AuthGuard],
    component: RegisterComponent,
  },
  {
    path: 'products',
    canActivate: [AuthGuard],
    component: ProductsComponent,
  },
  {
    path: 'customers',
    canActivate: [AuthGuard],
    component: CustomersComponent,
  },
  {
    path: "",
    redirectTo: "products",
    pathMatch: "full",
  },
  {
    path: 'madedProducts',
    canActivate: [AuthGuard],
    component: ProductOnStorageComponent,
  },

  {
    path: 'removedProducts',
    canActivate: [AuthGuard],
    component: RemovedProductsListComponent
  },
  {
    path: 'productOrders',
    canActivate: [AuthGuard],
    component: ProductOrdersComponent
  },

    {
    path: 'suppliers',
    canActivate: [AuthGuard],
    component: SuppliersComponent
  },
  {
    path: 'measures',
    canActivate: [AuthGuard],
    component: MeasureComponent,
  },
  {
    path: 'employees',
    canActivate: [AuthGuard],
    component: EmployeesComponent
  },
  {
    path: "removedProducts",
    canActivate: [AuthGuard],
    component: RemovedProductsListComponent,
  },
  {
    path: "productOrders",
    canActivate: [AuthGuard],
    component: ProductOrdersComponent,
  },
  {
    path: "rawStaffOrders",
    canActivate: [AuthGuard],
    component: RawStaffOrdersComponent,
  },
  {
    path: "outlay",
    canActivate: [AuthGuard],
    component: OutlayComponent,
  },
  {
    path: "incomes",
    canActivate: [AuthGuard],
    component: IncomesComponent,
  },
  {
    path: 'rawStaff',
    canActivate: [AuthGuard],
    component: RawStaffComponent,
  },
  {
    path: "rawStaffOnStorage",
    canActivate: [AuthGuard],
    component: RawStaffOnStorageComponent,
  },
  {
    path: "removedRawStaff",
    canActivate: [AuthGuard],
    component: RemovedRawStaffListComponent,
  },
  {
    path: "404",
    component: NotFoundComponent,
  },
  {
    path: "**",
    redirectTo: "404",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
