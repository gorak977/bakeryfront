import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app/app.component";
import { HeaderComponent } from "./components/header/header.component";
import { MaterialsModule } from "./shared/material.module";
import { ProductsListComponent } from "./components/products-components/products-list/products-list.component";
import { ProductDetailComponent } from "./components/products-components/product-detail/product-detail.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { CustomersListComponent } from "./components/customers-components/customers-list/customers-list.component";
import { CustomersComponent } from "./components/customers-components/customers-component/customers.component";
import { CustomerFormComponent } from "./components/customers-components/customer-form/customer-form.component";
import { ProductsFormComponent } from "./components/products-components/products-form/products-form.component";
import { ProductsComponent } from "./components/products-components/products/products.component";
import { MeasuresListComponent } from "./components/measure-components/measures-list/measures-list.component";
import { MeasureFormComponent } from "./components/measure-components/measure-form/measure-form.component";
import { MeasureComponent } from "./components/measure-components/measure/measure.component";
import { ProductOnStorageListComponent } from "./components/product-on-storage-components/product-on-storage-list/product-on-storage-list.component";
import { ProductOnStorageFormComponent } from "./components/product-on-storage-components/product-on-storage-form/product-on-storage-form.component";
import { ProductOnStorageComponent } from "./components/product-on-storage-components/product-on-storage/product-on-storage.component";
import { ProductOnStorageDetailComponent } from "./components/product-on-storage-components/product-on-storage-detail/product-on-storage-detail.component";
import { ProductOnStorageCreateComponent } from "./components/product-on-storage-components/product-on-storage-create/product-on-storage-create.component";
import { ProductOnStorageDeleteComponent } from './components/product-on-storage-components/product-on-storage-delete/product-on-storage-delete.component';
import { SupplierFormComponent } from './components/suppliers-components/supplier-form/supplier-form.component';
import { SuppliersListComponent } from './components/suppliers-components/suppliers-list/suppliers-list.component';
import { SuppliersComponent } from './components/suppliers-components/suppliers-component/suppliers.component';
import { EmployeesComponent } from './components/employees-components/employees/employees.component';
import { EmployeeFormComponent } from './components/employees-components/employee-form/employee-form.component';
import { EmployeesListComponent } from './components/employees-components/employees-list/employees-list.component';
import { RawStaffComponent } from './components/raw-staff-components/raw-staff/raw-staff.component';
import { RawStaffFormComponent } from './components/raw-staff-components/raw-staff-form/raw-staff-form.component';
import { RawStaffListComponent } from './components/raw-staff-components/raw-staff-list/raw-staff-list.component';
import { RemovedProductsListComponent } from './components/product-on-storage-components/removed-products-list/removed-products-list.component';
import { ProductsOrdersListComponent } from './components/product-orders-components/products-orders-list/products-orders-list.component';
import { ProductsOrderDetailComponent } from './components/product-orders-components/products-order-detail/products-order-detail.component';
import { ProductOrderFormComponent } from './components/product-orders-components/product-order-form/product-order-form.component';
import { ProductOrdersComponent } from './components/product-orders-components/product-orders/product-orders.component';
import { RawStaffOrdersComponent } from "./components/raw-staff-order-components/raw-staff-orders/raw-staff-orders.component";
import { RawStaffOrderFormComponent } from "./components/raw-staff-order-components/raw-staff-order-form/raw-staff-order-form.component";
import { RawStaffOrderDetailComponent } from "./components/raw-staff-order-components/raw-staff-order-detail/raw-staff-order-detail.component";
import { RawStaffOrdersListComponent } from "./components/raw-staff-order-components/raw-staff-orders-list/raw-staff-orders-list.component";
import { RawStaffOnStorageComponent } from "./components/raw-staff-on-storage-component/raw-staff-on-storage/raw-staff-on-storage.component";
import { RawStaffOnStorageListComponent } from "./components/raw-staff-on-storage-component/raw-staff-on-storage-list/raw-staff-on-storage-list.component";
import { RawStaffOnStorageDetailComponent } from "./components/raw-staff-on-storage-component/raw-staff-on-storage-detail/raw-staff-on-storage-detail.component";
import { RemovedRawStaffListComponent } from "./components/raw-staff-on-storage-component/removed-raw-staff-list/removed-raw-staff-list.component";
import { RawStaffOnStorageDeleteComponent } from "./components/raw-staff-on-storage-component/raw-staff-on-storage-delete/raw-staff-on-storage-delete.component";
import { OutlayComponent } from './components/financial-accounting-components/outlay-component/outlay.component';
import { IncomesComponent } from './components/financial-accounting-components/incomes-component/incomes.component';
import { LogInComponent } from './components/auth/log-in/log-in.component';
import { RegisterComponent } from './components/auth/register/register.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProductsListComponent,
    ProductDetailComponent,
    NotFoundComponent,
    CustomersListComponent,
    CustomerFormComponent,
    CustomersComponent,
    ProductsFormComponent,
    ProductsComponent,
    MeasuresListComponent,
    MeasureFormComponent,
    MeasureComponent,
    ProductOnStorageListComponent,
    ProductOnStorageFormComponent,
    ProductOnStorageComponent,
    ProductOnStorageDetailComponent,
    ProductOnStorageCreateComponent,
    ProductOnStorageDeleteComponent,
    SupplierFormComponent,
    SuppliersListComponent,
    SuppliersComponent,
    EmployeesComponent,
    EmployeeFormComponent,
    EmployeesListComponent,
    RawStaffComponent,
    RawStaffFormComponent,
    RawStaffListComponent,
    RemovedProductsListComponent,
    ProductsOrdersListComponent,
    ProductsOrderDetailComponent,
    ProductOrderFormComponent,
    ProductOrdersComponent,
    LogInComponent,
    RegisterComponent,
    RawStaffOrdersComponent,
    RawStaffOrderFormComponent,
    RawStaffOrderDetailComponent,
    RawStaffOrdersListComponent,
    RawStaffOnStorageComponent,
    RawStaffOnStorageListComponent,
    RawStaffOnStorageDetailComponent,
    RemovedRawStaffListComponent,
    RawStaffOnStorageDeleteComponent,
    OutlayComponent,
    IncomesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialsModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  entryComponents: [
    ProductDetailComponent,
    ProductOnStorageDetailComponent,
    ProductOnStorageCreateComponent,
    ProductOnStorageDeleteComponent,
    ProductsOrderDetailComponent,
    RawStaffOrderDetailComponent,
    RawStaffOnStorageDetailComponent,
    RawStaffOnStorageDeleteComponent,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
