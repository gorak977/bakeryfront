import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {
  logInForm: FormGroup;
  isError = false;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.createNewForm();
    if (this.authService.userRoles.length === 0) {
      this.authService.getUserRoles().subscribe(result => this.authService.userRoles = result);
    }
  }

  createNewForm() {
    this.logInForm = new FormGroup({
      userName: new FormControl(null, [Validators.required, Validators.maxLength(128)]),
      password: new FormControl(null, [Validators.required, Validators.maxLength(128)])
    });
  }

  logIn() {
    const user = this.getFormData();
    this.authService.logIn(user).subscribe(result => {
      if (result != null) {
        this.createNewForm();
        this.authService.user = result;
        const routs = this.authService.getRoutes();
        this.router.navigate([routs[0]]);
        this.isError = false;
      } else {
        this.isError = true;
      }
  });
  }

  getFormData(): User {
    const user: User = {
      userId: 0,
      userName: this.logInForm.value.userName,
      password: this.logInForm.value.password,
      role: 0
    };
    return user;
  }

}
