import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.interface';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  logInForm: FormGroup;
  isError = false;
  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.createNewForm();
    if (this.authService.userRoles.length === 0) {
      this.authService.getUserRoles().subscribe(result => this.authService.userRoles = result);
    }
  }

  createNewForm() {
    this.logInForm = new FormGroup({
      userName: new FormControl(null, [Validators.required, Validators.maxLength(128)]),
      password: new FormControl(null, [Validators.required, Validators.maxLength(128)]),
      role: new FormControl(null, Validators.required)
    });
  }

  register() {
    const user = this.getFormData();
    this.authService.registerUser(user).subscribe(result => {
      if (result != null) {
        this.createNewForm();
        const routs = this.authService.getRoutes();
        this.router.navigate([routs[0]]);
        this.isError = false;
      } else {
        this.isError = true;
      }
  });
  }

  getFormData(): User {
    const user: User = {
      userId: 0,
      userName: this.logInForm.value.userName,
      password: this.logInForm.value.password,
      role: this.logInForm.value.role
    };
    return user;
  }

}
