import { Component, OnInit } from '@angular/core';
import { ProductOrdersService } from 'src/app/services/product-orders.service';
import { ProductOrdersRequestService } from 'src/app/services/product-orders-request.service';
import { ProductsOrder } from 'src/app/models/productsOrder.interface';
import { MatDialog } from '@angular/material';
import { ProductsOrderDetailComponent } from '../products-order-detail/products-order-detail.component';

@Component({
  selector: 'app-products-orders-list',
  templateUrl: './products-orders-list.component.html',
  styleUrls: ['./products-orders-list.component.scss']
})
export class ProductsOrdersListComponent implements OnInit {

  constructor(
    public orderService: ProductOrdersService,
    private orderRequest: ProductOrdersRequestService,
    public dialog: MatDialog) { }
  displayedColumns: string[] = ['number', 'customerName', 'totalPrice', 'date', 'status'];

  ngOnInit() {
    this.updateProducts();
    this.orderService.orderInsertedSubject.subscribe(() => this.updateProducts());
  }

  updateProducts() {
    this.orderRequest.getAllOrders().subscribe(result => {
      this.orderService.productOrdersList = result;
     });
  }
  getRow(row: ProductsOrder) {
    this.orderRequest.getOrderItemsByOrderId(row.idOrder).subscribe(result => {
      this.orderService.orderItemsList = result;
      this.orderRequest.getSpecialStatuses().subscribe(statuses => {
        this.orderService.getOrderStatus(statuses, row);
        this.openDialog(row);
      });
    });
  }

  openDialog(row: ProductsOrder ): void {
    const dialogRef = this.dialog.open(ProductsOrderDetailComponent, {
      data: row
    });
  }
}
