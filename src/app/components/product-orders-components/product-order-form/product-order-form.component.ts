import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { ProductOrdersService } from 'src/app/services/product-orders.service';
import { ProductOrdersRequestService } from 'src/app/services/product-orders-request.service';
import { ProductsOrder } from 'src/app/models/productsOrder.interface';
import { ProductOrderItem } from 'src/app/models/productOrderItem.interface';

@Component({
  selector: 'app-product-order-form',
  templateUrl: './product-order-form.component.html',
  styleUrls: ['./product-order-form.component.scss']
})
export class ProductOrderFormComponent implements OnInit {
  createOrderForm: FormGroup;
  constructor(private orderRequest: ProductOrdersRequestService, public orderService: ProductOrdersService) { }

  ngOnInit() {
    this.createNewForm();

    this.orderService.onUpdateClickSubject.subscribe(result => {
      this.orderService.orderToUpdate = result;
      this.setOrderToForm(result);
      this.setOrderItemsToForm(this.orderService.orderItemsList);
    });
  }

  createNewForm() {
    this.createOrderForm = new FormGroup({
      idCustomer: new FormControl(null, Validators.required),
      orderItems: new FormArray([])
    });

    this.orderRequest.getAllCustomers().subscribe(result =>
      this.orderService.customerSelect = result);
  }
  getAutocompleteProductArray(name: string) {
    this.orderRequest.getProductAutocomplete(name).subscribe(result => {
        this.orderService.productAutocomplete = result;
      });
  }
  addOrderItem() {
    const control = new FormGroup({
      product: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      amount: new FormControl(null, Validators.required),
      idProduct: new FormControl(null)
    });
    control.get('product').valueChanges.subscribe(name => {
      if (name !== '') {
      this.getAutocompleteProductArray(name);
      }
    });
    (this.createOrderForm.get('orderItems') as FormArray).push(control);
  }

  deleteOrderItem(index: number) {
    (this.createOrderForm.get('orderItems') as FormArray).removeAt(index);
  }

  getOrderFromForm(): ProductsOrder {
    const order: ProductsOrder = {
      idOrder: 0,
      idOrderStatus: 0,
      customer: null,
      time: null,
      totalPrice: 0,
      status: null,
      idCustomer: this.createOrderForm.value.idCustomer
    };
    return order;
  }

  setOrderToForm(order: ProductsOrder) {
    this.createOrderForm.patchValue({
      idCustomer: order.idCustomer
    });
    if (this.orderService.orderToUpdate !== null) {
      this.createOrderForm.get('idCustomer').disable();
    }
  }

  setOrderItemsToForm(orderItems: ProductOrderItem[]) {
    (this.createOrderForm.get('orderItems') as FormArray).clear();
    let i = 0;
    orderItems.forEach(order => {
      this.addOrderItem();
      if (this.orderService.orderToUpdate !== null) {
        (this.createOrderForm.get('orderItems') as FormArray).controls[i].get('product').disable();
      }
      (this.createOrderForm.get('orderItems') as FormArray).controls[i].patchValue({
        product: orderItems[i].product.productName,
        amount: orderItems[i].amount,
        idProduct: orderItems[i].product.idProduct
      });
      i++;
    });
  }

  getOrderItemsFromForm(): ProductOrderItem[] {
    const orderItems: ProductOrderItem[] = [];
    this.createOrderForm.value.orderItems.forEach(element => {
      const item: ProductOrderItem = {
        idProductOrderItem: 0,
        idOrder: 0,
        product: null,
        order: null,
        amount: Number(element.amount),
        idProduct: Number(element.idProduct)
      };
      orderItems.push(item);
    });
    return orderItems;
  }

  createOrder() {
    const order = this.getOrderFromForm();
    const orderItems = this.getOrderItemsFromForm();
    this.orderRequest.setNewOrder(order).subscribe(result => {
      orderItems.forEach(item => item.idOrder = result.idOrder, error => this.orderRequest.cancelOrder(result.idOrder).subscribe());
      this.orderRequest.setNewOrderItems(orderItems).subscribe(() =>
      this.orderService.orderInsertedSubject.next());
    });
    this.createNewForm();
  }


  autocompleteProductsClick(arrayIndex, autocompleteIndex) {
    (this.createOrderForm.get('orderItems') as FormArray).controls[arrayIndex].patchValue({
      idProduct: this.orderService.productAutocomplete[autocompleteIndex].idProduct
    });
    this.orderService.productAutocomplete = [];
  }

  cancelButtonClick() {
    this.createNewForm();
    this.orderService.orderItemsList = [];
    this.orderService.orderToUpdate = null;
  }

  updateButtonClick() {
    const order = this.getOrderFromForm();
    order.idOrder = this.orderService.orderToUpdate.idOrder;
    order.idOrderStatus = this.orderService.orderToUpdate.idOrderStatus;
    order.idCustomer = this.orderService.orderToUpdate.idCustomer;
    order.time = this.orderService.orderToUpdate.time;
    const orderItems = this.getOrderItemsFromForm();
    let i = 0;
    orderItems.forEach(item => {
      if (i <= this.orderService.orderItemsList.length - 1) {
      item.idProductOrderItem = this.orderService.orderItemsList[i].idProductOrderItem;
      } else {
        item.idProductOrderItem = 0;
      }
      item.idOrder = this.orderService.orderToUpdate.idOrder;
      i++;
    });
    this.orderRequest.updateOrder(order).subscribe(result => {
      this.orderRequest.updateOrderItemRange(orderItems)
      .subscribe( () => this.orderService.orderInsertedSubject.next());
    });
    this.orderService.orderToUpdate = null;
    this.orderService.orderItemsList = [];
    this.createNewForm();

  }

}
