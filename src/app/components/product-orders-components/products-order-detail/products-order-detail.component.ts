import { Component, OnInit, Inject } from '@angular/core';
import { ProductsOrder } from 'src/app/models/productsOrder.interface';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProductOrdersService } from 'src/app/services/product-orders.service';
import { ProductOrdersRequestService } from 'src/app/services/product-orders-request.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products-order-detail',
  templateUrl: './products-order-detail.component.html',
  styleUrls: ['./products-order-detail.component.scss']
})
export class ProductsOrderDetailComponent {

  order: ProductsOrder;
  constructor(
    public dialogRef: MatDialogRef<ProductsOrderDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ProductsOrder,
    public orderService: ProductOrdersService,
    public orderRequestService: ProductOrdersRequestService,
    private router: Router) {
      this.order = data;
  }
  isProductNotEnough = false;
  displayedColumns: string[] = ['number', 'productName', 'amount', 'price'];

  onCloseClick(): void {
    this.orderService.orderItemsList = [];
    this.dialogRef.close();
    this.orderService.isOrderInProgress = true;
    this.isProductNotEnough = false;
  }
  updateButtonClick() {
    this.isProductNotEnough = false;
    this.orderService.onUpdateClickSubject.next(this.order);
    this.dialogRef.close();
    this.orderService.isOrderInProgress = true;
  }
  cancelButtonClick() {
    this.isProductNotEnough = false;
    this.orderRequestService.cancelOrder(this.order.idOrder).subscribe(() =>
    this.orderService.orderInsertedSubject.next());
    this.onCloseClick();
  }
  executeOrderButtonClick() {
    this.isProductNotEnough = false;
    this.orderRequestService.confirmOrder(this.order.idOrder).subscribe(() => {
      this.onCloseClick();
      this.orderService.orderInsertedSubject.next();
    }, error => {
      this.isProductNotEnough = true;
    });
  }
}
