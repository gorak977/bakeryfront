import { Component, OnInit, Inject } from "@angular/core";
import { RawStaffOnStorage } from "src/app/models/rawStaffOnStorage.interface";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { RawStaffOnStorageDetailComponent } from "../raw-staff-on-storage-detail/raw-staff-on-storage-detail.component";
import { RawStaffOnStorageService } from "src/app/services/raw-staff-on-storage.service";
import { RawStaffOnStorageRequestService } from "src/app/services/raw-staff-on-storage-request.service";
import { EmployeesRequestService } from "src/app/services/employees-request.service";
import { RemovedRawStaff } from "src/app/models/removedRawStaff.interface";

@Component({
  selector: "app-raw-staff-on-storage-delete",
  templateUrl: "./raw-staff-on-storage-delete.component.html",
  styleUrls: ["./raw-staff-on-storage-delete.component.scss"],
})
export class RawStaffOnStorageDeleteComponent implements OnInit {
  rawStaff: RawStaffOnStorage;
  isRawStaffNotEnough = false;
  deleteRawStaffOnStorageForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<RawStaffOnStorageDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RawStaffOnStorage,
    public rawStaffOnStorageService: RawStaffOnStorageService,
    public rawStaffOnStorageRequest: RawStaffOnStorageRequestService,
    private employeeRequestService: EmployeesRequestService
  ) {
    this.rawStaff = data;
  }
  ngOnInit() {
    this.createNewForm();
    this.employeeRequestService
      .getAllEmployees()
      .subscribe(
        (result) => (this.rawStaffOnStorageService.employeeSelect = result)
      );
  }

  createNewForm() {
    this.deleteRawStaffOnStorageForm = new FormGroup({
      reasonToRemove: new FormControl(null, [
        Validators.required,
        Validators.maxLength(20),
      ]),
      employeeId: new FormControl(null),
      amount: new FormControl(null, Validators.required),
    });

    this.deleteRawStaffOnStorageForm
      .get("amount")
      .valueChanges.subscribe((value) => {
        if (value > this.rawStaff.amount) {
          this.isRawStaffNotEnough = true;
        } else {
          this.isRawStaffNotEnough = false;
        }
      });
  }
  removeProductOnStorage() {
    const rawStaffToRemove: RemovedRawStaff = {
      idRemovedRawStaff: 0,
      idEmployee: this.deleteRawStaffOnStorageForm.value.employeeId,
      idRawStaff: this.rawStaff.idRawStaff,
      amount: this.deleteRawStaffOnStorageForm.value.amount,
      reasonToRemove: this.deleteRawStaffOnStorageForm.value.reasonToRemove,
      removingDate: null,
      rawStaff: null,
      employee: null,
    };

    this.rawStaffOnStorageRequest
      .removeRawStaffFromStorage(
        rawStaffToRemove,
        this.rawStaff.idRawStaffOnStorage
      )
      .subscribe(() => {
        this.rawStaffOnStorageService.rawStaffInsertedSubject.next();
        this.rawStaffOnStorageRequest
          .getAllRawStaffOnStorage()
          .subscribe((result) => {
            this.rawStaffOnStorageService.rawStaffOnStorageList = result;
          });
      });

    this.dialogRef.close();
  }

  cancelButtonClick() {
    this.dialogRef.close();
  }
}
