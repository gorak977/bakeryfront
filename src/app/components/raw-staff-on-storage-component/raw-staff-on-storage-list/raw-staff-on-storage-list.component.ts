import { Component, OnInit } from "@angular/core";
import { RawStaffOnStorageRequestService } from "src/app/services/raw-staff-on-storage-request.service";
import { RawStaffOnStorageService } from "src/app/services/raw-staff-on-storage.service";
import { MatDialog } from "@angular/material";
import { RawStaffOnStorage } from "src/app/models/rawStaffOnStorage.interface";
import { RawStaffOnStorageDetailComponent } from "../raw-staff-on-storage-detail/raw-staff-on-storage-detail.component";
import { DatePipe } from "@angular/common";

@Component({
  selector: "app-raw-staff-on-storage-list",
  templateUrl: "./raw-staff-on-storage-list.component.html",
  styleUrls: ["./raw-staff-on-storage-list.component.scss"],
  providers: [DatePipe],
})
export class RawStaffOnStorageListComponent implements OnInit {
  constructor(
    private rawStaffOnStorageRequest: RawStaffOnStorageRequestService,
    public rawStaffOnStorageService: RawStaffOnStorageService,
    public dialog: MatDialog,
    public datepipe: DatePipe
  ) {}

  displayedColumns: string[] = [
    "number",
    "rawStaffName",
    "amount",
    "measure",
    "consumeUntil",
  ];
  ngOnInit() {
    this.updateRawStaff();
  }

  updateRawStaff() {
    this.rawStaffOnStorageRequest
      .getAllRawStaffOnStorage()
      .subscribe((result) => {
        this.rawStaffOnStorageService.rawStaffOnStorageList = result;
      });
  }

  getRow(row) {
    this.openDialog(row);
  }

  openDialog(row: RawStaffOnStorage): void {
    const dialogRef = this.dialog.open(RawStaffOnStorageDetailComponent, {
      data: row,
    });
  }
}
