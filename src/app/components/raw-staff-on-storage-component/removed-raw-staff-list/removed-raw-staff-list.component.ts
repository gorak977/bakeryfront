import { Component, OnInit } from '@angular/core';
import { RawStaffOnStorageRequestService } from 'src/app/services/raw-staff-on-storage-request.service';
import { RawStaffOnStorageService } from 'src/app/services/raw-staff-on-storage.service';

@Component({
  selector: 'app-removed-products-list',
  templateUrl: './removed-raw-staff-list.component.html',
  styleUrls: ['./removed-raw-staff-list.component.scss']
})
export class RemovedRawStaffListComponent implements OnInit {

  constructor(
    private rawStaffOnStorageRequest: RawStaffOnStorageRequestService,
    public rawStaffOnStorageService: RawStaffOnStorageService) { }

    displayedColumns: string[] = ['number', 'rawStaffName', 'amount', 'reasonToRemove', 'removingDate', 'removedBy'];

  ngOnInit() {
    this.updateProducts();
    this.rawStaffOnStorageService.rawStaffRemovedSubject.subscribe(() => this.updateProducts());
  }
  updateProducts() {
    this.rawStaffOnStorageRequest.getAllRemovedRawStaff().subscribe(result => {
      this.rawStaffOnStorageService.removedRawStaffList = result;
     });
  }
}
