import { Component, OnInit, Inject } from "@angular/core";
import { RawStaffOnStorage } from "src/app/models/rawStaffOnStorage.interface";
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from "@angular/material";
import { RawStaffOnStorageRequestService } from "src/app/services/raw-staff-on-storage-request.service";
import { RawStaffOnStorageService } from "src/app/services/raw-staff-on-storage.service";
import { RawStaffOnStorageDeleteComponent } from "../raw-staff-on-storage-delete/raw-staff-on-storage-delete.component";

@Component({
  selector: "app-raw-staff-on-storage-detail",
  templateUrl: "./raw-staff-on-storage-detail.component.html",
  styleUrls: ["./raw-staff-on-storage-detail.component.scss"],
})
export class RawStaffOnStorageDetailComponent implements OnInit {
  rawStaff: RawStaffOnStorage;

  constructor(
    public dialogRef: MatDialogRef<RawStaffOnStorageDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RawStaffOnStorage,
    public productOnStorageService: RawStaffOnStorageService,
    public productOnStorageRequest: RawStaffOnStorageRequestService,
    public dialog: MatDialog
  ) {
    this.rawStaff = data;
  }

  ngOnInit(): void {}
  deleteButtonClick() {
    this.dialogRef.close();
    this.openDialog();
  }
  onCloseClick() {
    this.dialogRef.close();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(RawStaffOnStorageDeleteComponent, {
      data: this.rawStaff,
    });
  }
}
