import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";

import { FinancialAccountingService } from "src/app/services/financial-accounting.service";
import { RawStaffOrdersRequestService } from "src/app/services/raw-staff-orders-request.service";
import { RawStaffOrderDetailComponent } from "../../raw-staff-order-components/raw-staff-order-detail/raw-staff-order-detail.component";
import { RawStaffOrdersService } from "src/app/services/raw-staff-orders.service";
import { ProductOrdersRequestService } from "src/app/services/product-orders-request.service";
import { ProductOrdersService } from "src/app/services/product-orders.service";
import { RawStaffOrder } from "src/app/models/rawStaffOrder.interface";
import { FinancialAccountingRequestService } from "src/app/services/financial-accounting-request.service";

@Component({
  selector: "app-outlay-component",
  templateUrl: "./outlay.component.html",
  styleUrls: ["./outlay.component.scss"],
})
export class OutlayComponent implements OnInit {
  constructor(
    private financeRequest: FinancialAccountingRequestService,
    public financeService: FinancialAccountingService,
    private rawStaffOrderRequest: RawStaffOrdersRequestService,
    private rawStaffOrderService: RawStaffOrdersService,
    public dialog: MatDialog
  ) {}

  checked = false;

  displayedColumns: string[] = [
    "idOrder",
    "supplierName",
    "totalPrice",
    "date",
  ];

  displayedEmployeeColumns: string[] = [
    "name",
    "surname",
    "salary",
    "position",
  ];

  ngOnInit() {
    this.financeRequest.getOutlays().subscribe((result) => {
      this.financeService.outlay = result;
    });
  }

  updateRawStaff() {
    this.rawStaffOrderRequest.getAllOrders().subscribe((result) => {
      this.rawStaffOrderService.rawStaffOrdersList = result;
    });
  }
  getRow(row: RawStaffOrder) {
    this.rawStaffOrderRequest
      .getOrderItemsByOrderId(row.idOrder)
      .subscribe((result) => {
        this.rawStaffOrderService.orderItemsList = result;
        this.openDialog(row);
      });
  }

  openDialog(row: RawStaffOrder): void {
    const dialogRef = this.dialog.open(RawStaffOrderDetailComponent, {
      data: row,
    });
  }
}
