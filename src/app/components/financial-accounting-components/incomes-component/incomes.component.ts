import { Component, OnInit } from "@angular/core";
import { FinancialAccountingRequestService } from "src/app/services/financial-accounting-request.service";
import { FinancialAccountingService } from "src/app/services/financial-accounting.service";
import { ProductOrdersRequestService } from "src/app/services/product-orders-request.service";
import { ProductOrdersService } from "src/app/services/product-orders.service";
import { MatDialog } from "@angular/material";
import { ProductsOrder } from "src/app/models/productsOrder.interface";
import { ProductsOrderDetailComponent } from "../../product-orders-components/products-order-detail/products-order-detail.component";

@Component({
  selector: "app-incomes-component",
  templateUrl: "./incomes.component.html",
  styleUrls: ["./incomes.component.scss"],
})
export class IncomesComponent implements OnInit {
  constructor(
    private financeRequest: FinancialAccountingRequestService,
    public financeService: FinancialAccountingService,
    private productOrderRequest: ProductOrdersRequestService,
    private productOrderService: ProductOrdersService,
    public dialog: MatDialog
  ) {}

  checked = false;

  displayedColumns: string[] = [
    "idOrder",
    "customerName",
    "totalPrice",
    "netIncome",
    "date",
  ];

  ngOnInit() {
    this.financeRequest.getIncomes().subscribe((result) => {
      this.financeService.incomes = result;
      console.log(this.financeService.incomes);
    });
  }

  updateRawStaff() {
    this.productOrderRequest.getAllOrders().subscribe((result) => {
      this.productOrderService.productOrdersList = result;
    });
  }
  getRow(row: ProductsOrder) {
    this.productOrderRequest
      .getOrderItemsByOrderId(row.idOrder)
      .subscribe((result) => {
        this.productOrderService.orderItemsList = result;
        this.openDialog(row);
      });
  }

  openDialog(row: ProductsOrder): void {
    const dialogRef = this.dialog.open(ProductsOrderDetailComponent, {
      data: row,
    });
  }
}
