import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { RawStaffRequestService } from "src/app/services/raw-staff-request.service";
import { RawStaffService } from "src/app/services/raw-staff.service";
import { MeasureRequestService } from "src/app/services/measure-request.service";
import { MeasureService } from "src/app/services/measure.service";
import { Employee } from "src/app/models/employee.interface";
import { Measure } from "src/app/models/measure.interface";
import { RawStaff } from "src/app/models/rawStaff.interface";

@Component({
  selector: "app-raw-staff-form",
  templateUrl: "./raw-staff-form.component.html",
  styleUrls: ["./raw-staff-form.component.scss"],
})
export class RawStaffFormComponent implements OnInit {
  createRawStaffForm: FormGroup;
  constructor(
    private rawStaffRequest: RawStaffRequestService,
    public rawStaffService: RawStaffService,
    private measureRequest: MeasureRequestService,
    public measureService: MeasureService
  ) {}

  ngOnInit() {
    this.rawStaffService.rawStaffToUpdate = null;
    
    this.createRawStaffForm = new FormGroup({
      name: new FormControl(null, [
        Validators.required,
        Validators.maxLength(20),
      ]),
      expirationDate: new FormControl(null, [
        Validators.required,
        Validators.pattern("^[0-9]*"),
      ]),
      pricePerMeasure: new FormControl(null, [
        Validators.required,
        Validators.pattern("^[0-9]*[.,]?[0-9]+$"),
      ]),
      idMeasure: new FormControl(null, Validators.required),
    });

    this.rawStaffService.onUpdateClickSubject.subscribe((rawStaff) => {
      this.setFormData(rawStaff);
      this.rawStaffService.rawStaffToUpdate = rawStaff;
    });
    this.onLoad();
  }

  onLoad() {
    this.updateMeasures();
  }

  updateMeasures() {
    this.measureRequest.getAllMeasures().subscribe((measures: Measure[]) => {
      this.measureService.measureList = measures;
    });
  }

  createRawStaff() {
    const rawStaff: RawStaff = this.getFormData();
    this.rawStaffRequest
      .setNewRawStaff(rawStaff)
      .subscribe((result) =>
        this.rawStaffService.rawStaffInsertedSubject.next()
      );
    console.log(rawStaff);
  }

  getFormData(): RawStaff {
    const rawStaff: RawStaff = {
      idRawStaff: 0,
      name: this.createRawStaffForm.value.name,
      expirationDate: this.createRawStaffForm.value.expirationDate,
      pricePerMeasure: this.createRawStaffForm.value.pricePerMeasure,
      idMeasure: this.createRawStaffForm.value.idMeasure,
      measureName: "",
    };
    return rawStaff;
  }

  setFormData(rawStaff: RawStaff) {
    this.createRawStaffForm.setValue({
      name: rawStaff.name,
      expirationDate: rawStaff.expirationDate,
      pricePerMeasure: rawStaff.pricePerMeasure,
      idMeasure: rawStaff.idMeasure,
    });
  }

  cancelButtonClick() {
    if (this.rawStaffService.rawStaffToUpdate != null) {
      this.rawStaffService.rawStaffToUpdate = null;
    }
    this.createRawStaffForm.reset();
  }

  updateButtonClick() {
    const rawStaff = this.getFormData();
    rawStaff.idRawStaff = this.rawStaffService.rawStaffToUpdate.idRawStaff;

    this.rawStaffRequest.updateRawStaff(rawStaff).subscribe((result) => {
      this.rawStaffService.rawStaffInsertedSubject.next();
      this.rawStaffService.rawStaffToUpdate = null;
      this.createRawStaffForm.reset();
    });
  }

  deleteButtonClick() {
    this.rawStaffRequest
      .deleteRawStaff(this.rawStaffService.rawStaffToUpdate.idRawStaff)
      .subscribe((result) => {
        this.rawStaffService.rawStaffInsertedSubject.next();
        this.rawStaffService.rawStaffToUpdate = null;
        this.createRawStaffForm.reset();
      });
  }
}
