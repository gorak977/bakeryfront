import { Component, OnInit } from '@angular/core';
import { RawStaffService } from 'src/app/services/raw-staff.service';
import { RawStaffRequestService } from 'src/app/services/raw-staff-request.service';

@Component({
  selector: 'app-raw-staff-list',
  templateUrl: './raw-staff-list.component.html',
  styleUrls: ['./raw-staff-list.component.scss']
})
export class RawStaffListComponent implements OnInit {

  constructor(public rawStaffService: RawStaffService, private rawStaffRequest: RawStaffRequestService) { }

  displayedColumns: string[] = ['number', 'rawStaffName', 'expirationDate', 'pricePerMeasure', 'measureName'];

  ngOnInit() {
    this.onLoad();
  }

  onLoad() {
    this.updateList();

    this.rawStaffService.rawStaffInsertedSubject.subscribe(() => this.updateList());
  }

  updateList() {
    this.rawStaffRequest.getAllRawStaff().subscribe(result => {
      this.rawStaffService.rawStaffList = result;
    });
  }

  getRow(rawStaff) {
    this.rawStaffService.onUpdateClickSubject.next(rawStaff);
  }
}
