import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/services/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit {
  public routs: { routeName: string; routePath: string }[] = [
    {
      routeName: "products",
      routePath: "products",
    },
    {
      routeName: "measures",
      routePath: "measures",
    },
    {
      routeName: "maded products",
      routePath: "madedProducts",
    },
    {
      routeName: "customers",
      routePath: "customers",
    },
    {
      routeName: "removed products",
      routePath: "removedProducts",
    },
    {
      routeName: "product orders",
      routePath: "productOrders",
    },
    {
      routeName: "suppliers",
      routePath: "suppliers",
    },
    {
      routeName: "employees",
      routePath: "employees",
    },
    {
      routeName: "raw staff orders",
      routePath: "rawStaffOrders",
    },
    {
      routeName: "outlay",
      routePath: "outlay",
    },
    {
      routeName: "incomes",
      routePath: "incomes",
    },
    {
      routeName: "raw staff on storage",
      routePath: "rawStaffOnStorage",
    },
    {
      routeName: "raw staff",
      routePath: "rawStaff",
    },
    {
      routeName: "removed raw staff",
      routePath: "removedRawStaff",
    },
  ];
  currentRouts: { routeName: string; routePath: string }[] = [];

  constructor(public auth: AuthService, private router: Router) {}

  ngOnInit() {
    const tempRouts = this.auth.getRoutes();
    console.log(tempRouts);
    this.routs.forEach((rout) => {
      if (tempRouts.includes(rout.routePath)) {
        this.currentRouts.push(rout);
      }
    });
  }

  logOut() {
    this.auth.logOut();
  }
  registerUser() {
    this.router.navigate(["register"]);
  }
}
