import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, FormArray, Validators } from "@angular/forms";
import { RawStaffOrdersRequestService } from "src/app/services/raw-staff-orders-request.service";
import { RawStaffOrdersService } from "src/app/services/raw-staff-orders.service";
import { RawStaffOrder } from "src/app/models/rawStaffOrder.interface";
import { RawStaffOrderItem } from "src/app/models/rawStaffOrderItem.interface";
import { RawStaff } from "src/app/models/rawStaff.interface";

@Component({
  selector: "app-raw-staff-order-form",
  templateUrl: "./raw-staff-order-form.component.html",
  styleUrls: ["./raw-staff-order-form.component.scss"],
})
export class RawStaffOrderFormComponent implements OnInit {
  createOrderForm: FormGroup;
  orderTotalPrice: number = null;

  constructor(
    private orderRequest: RawStaffOrdersRequestService,
    public orderService: RawStaffOrdersService
  ) {}

  ngOnInit() {
    this.createNewForm();

    this.orderService.onUpdateClickSubject.subscribe((result) => {
      this.orderService.orderToUpdate = result;
      this.setOrderToForm(result);
      this.setOrderItemsToForm(this.orderService.orderItemsList);
    });
  }

  createNewForm() {
    this.createOrderForm = new FormGroup({
      idSupplier: new FormControl(null, Validators.required),
      orderItems: new FormArray([]),
    });

    this.orderRequest
      .getAllSuppliers()
      .subscribe((result) => (this.orderService.supplierSelect = result));
  }

  updateRawStaffList(supplierID: number) {
    this.orderRequest.getAllSupplierRawStaff(supplierID).subscribe((result) => {
      this.orderService.rawStaffList = result;
    });
    this.createOrderForm.get("orderItems").reset();
  }

  addOrderItem() {
    const control = new FormGroup({
      amount: new FormControl(null, Validators.required),
      rawStaff: new FormControl(null, Validators.required),
    });
    (this.createOrderForm.get("orderItems") as FormArray).push(control);
  }

  deleteOrderItem(index: number) {
    (this.createOrderForm.get("orderItems") as FormArray).removeAt(index);
    this.calculateOrderTotalPrice();
  }

  getOrderFromForm(): RawStaffOrder {
    const order: RawStaffOrder = {
      idOrder: 0,
      idOrderStatus: 0,
      supplier: null,
      time: null,
      totalPrice: 0,
      status: null,
      idSupplier: this.createOrderForm.value.idSupplier,
    };
    return order;
  }

  setOrderToForm(order: RawStaffOrder) {
    this.createOrderForm.patchValue({
      idSupplier: order.idSupplier,
    });
    this.updateRawStaffList(order.idSupplier);
    if (this.orderService.orderToUpdate !== null) {
      this.createOrderForm.get("idSupplier").disable();
    }
  }

  setOrderItemsToForm(orderItems: RawStaffOrderItem[]) {
    (this.createOrderForm.get("orderItems") as FormArray).clear();
    let i = 0;
    orderItems.forEach((item) => {
      this.addOrderItem();
      (this.createOrderForm.get("orderItems") as FormArray).controls[
        i
      ].patchValue({
        rawStaff: orderItems[i].rawStaff,
        amount: orderItems[i].amount,
      });
      i++;
    });
    this.calculateOrderTotalPrice();
  }

  getOrderItemsFromForm(): RawStaffOrderItem[] {
    const orderItems: RawStaffOrderItem[] = [];
    this.createOrderForm.value.orderItems.forEach((element) => {
      const item: RawStaffOrderItem = {
        idRawStaffOrderItem: 0,
        idOrder: 0,
        rawStaff: null,
        order: null,
        amount: Number(element.amount),
        idRawStaff: Number(element.rawStaff.idRawStaff),
      };
      orderItems.push(item);
    });
    return orderItems;
  }

  createOrder() {
    const order = this.getOrderFromForm();
    const orderItems = this.getOrderItemsFromForm();
    this.orderRequest.setNewOrder(order).subscribe((result) => {
      orderItems.forEach((item) => (item.idOrder = result.idOrder));
      this.orderRequest
        .setNewOrderItems(orderItems)
        .subscribe(() => this.orderService.orderInsertedSubject.next());
    });
    this.createNewForm();
    this.orderService.orderItemsList = [];
  }

  cancelButtonClick() {
    this.createOrderForm.get("idSupplier").enable();
    this.createNewForm();
    this.orderService.orderItemsList = [];
    this.orderService.orderToUpdate = null;
    this.orderTotalPrice = null;
  }

  updateButtonClick() {
    const order = this.getOrderFromForm();
    order.idOrder = this.orderService.orderToUpdate.idOrder;
    order.idOrderStatus = this.orderService.orderToUpdate.idOrderStatus;
    order.idSupplier = this.orderService.orderToUpdate.idSupplier;
    order.totalPrice = this.orderService.orderToUpdate.totalPrice;
    order.time = this.orderService.orderToUpdate.time;
    const orderItems = this.getOrderItemsFromForm();
    console.log(orderItems);
    let i = 0;
    orderItems.forEach((item) => {
      if (i <= this.orderService.orderItemsList.length - 1) {
        item.idRawStaffOrderItem = this.orderService.orderItemsList[
          i
        ].idRawStaffOrderItem;
      } else {
        item.idRawStaffOrderItem = 0;
      }
      item.idOrder = this.orderService.orderToUpdate.idOrder;
      i++;
    });
    this.orderRequest.updateOrder(order).subscribe((result) => {
      this.orderRequest
        .updateOrderItemRange(orderItems)
        .subscribe(() => this.orderService.orderInsertedSubject.next());
    });
    this.orderService.orderToUpdate = null;
    this.orderService.orderItemsList = [];
    this.createOrderForm.get("idSupplier").enable();
    this.createNewForm();
  }

  calculateOrderTotalPrice() {
    this.orderTotalPrice = 0;
    this.createOrderForm.value.orderItems.forEach((element) => {
      this.orderTotalPrice += element.amount * element.rawStaff.pricePerMeasure;
    });
  }
}
