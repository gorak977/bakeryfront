import { Component, OnInit, ViewChild } from "@angular/core";
import { RawStaffOrdersService } from "src/app/services/raw-staff-orders.service";
import { RawStaffOrdersRequestService } from "src/app/services/raw-staff-orders-request.service";
import { MatDialog, MatTableDataSource, MatSort } from "@angular/material";
import { RawStaffOrder } from "src/app/models/rawStaffOrder.interface";
import { RawStaffOrderDetailComponent } from "../raw-staff-order-detail/raw-staff-order-detail.component";

@Component({
  selector: "app-raw-staff-orders-list",
  templateUrl: "./raw-staff-orders-list.component.html",
  styleUrls: ["./raw-staff-orders-list.component.scss"],
})
export class RawStaffOrdersListComponent implements OnInit {
  constructor(
    public orderService: RawStaffOrdersService,
    private orderRequest: RawStaffOrdersRequestService,
    public dialog: MatDialog
  ) {}
  displayedColumns: string[] = [
    "number",
    "supplierName",
    "totalPrice",
    "date",
    "status",
  ];
  dataSource = new MatTableDataSource(this.orderService.rawStaffOrdersList);

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.updateRawStaff();
    this.orderService.orderInsertedSubject.subscribe(() =>
      this.updateRawStaff()
    );
  }

  updateRawStaff() {
    this.orderRequest.getAllOrders().subscribe((result) => {
      this.orderService.rawStaffOrdersList = result;
    });
  }
  getRow(row: RawStaffOrder) {
    this.orderRequest
      .getOrderItemsByOrderId(row.idOrder)
      .subscribe((result) => {
        this.orderService.orderItemsList = result;
        this.openDialog(row);
      });
  }

  openDialog(row: RawStaffOrder): void {
    const dialogRef = this.dialog.open(RawStaffOrderDetailComponent, {
      data: row,
    });
  }
}
