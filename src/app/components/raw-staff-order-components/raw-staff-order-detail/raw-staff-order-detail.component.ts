import { Component, OnInit, Inject } from "@angular/core";
import { RawStaffOrder } from "src/app/models/rawStaffOrder.interface";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { RawStaffOrdersService } from "src/app/services/raw-staff-orders.service";
import { RawStaffOrdersRequestService } from "src/app/services/raw-staff-orders-request.service";
import { Router } from "@angular/router";
import { FormControl, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-raw-staff-order-detail",
  templateUrl: "./raw-staff-order-detail.component.html",
  styleUrls: ["./raw-staff-order-detail.component.scss"],
})
export class RawStaffOrderDetailComponent implements OnInit {
  order: RawStaffOrder;
  storageForm: FormGroup;
  errorMessage: string;
  constructor(
    public dialogRef: MatDialogRef<RawStaffOrderDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RawStaffOrder,
    public orderService: RawStaffOrdersService,
    public orderRequestService: RawStaffOrdersRequestService,
    private router: Router
  ) {
    this.order = data;
  }
  displayedColumns: string[] = [
    "number",
    "rawStaffName",
    "amount",
    "pricePerMeasure",
    "price",
  ];

  ngOnInit() {
    this.createNewForm();
    this.orderRequestService.getAllStorages().subscribe((storages) => {
      this.orderService.storagesList = storages;
    });
    this.checkOrderAbilities();
  }

  checkOrderAbilities() {
    this.orderRequestService
      .checkAbilityToUpdate(this.order.idOrder)
      .subscribe((ability) => {
        this.orderService.abilityToUpdateOrder = ability;
      });
    this.orderRequestService
      .checkAbilityToCancel(this.order.idOrder)
      .subscribe((ability) => {
        this.orderService.abilityToCancelOrder = ability;
      });
    this.orderRequestService
      .checkAbilityToConfirm(this.order.idOrder)
      .subscribe((ability) => {
        this.orderService.abilityToConfirmOrder = ability;
      });
  }

  updateOrderDetails() {
    this.orderRequestService
      .getOrder(this.order.idOrder)
      .subscribe((result) => {
        this.order = result;
      });
  }

  createNewForm() {
    this.storageForm = new FormGroup({
      idStorage: new FormControl(null, Validators.required),
    });
  }

  onCloseClick(): void {
    this.orderService.orderItemsList = [];
    this.dialogRef.close();
    this.orderService.isOrderInProgress = true;
  }
  updateButtonClick() {
    this.orderService.onUpdateClickSubject.next(this.order);
    this.dialogRef.close();
    this.orderService.isOrderInProgress = true;
  }
  cancelButtonClick() {
    this.orderRequestService
      .cancelOrder(this.order.idOrder)
      .subscribe(() => this.orderService.orderInsertedSubject.next());
    this.onCloseClick();
  }
  confirmOrderButtonClick() {
    this.orderRequestService
      .confirmOrder(this.order.idOrder, this.storageForm.value.idStorage)
      .subscribe(
        () => {
          this.updateOrderDetails();
          this.orderService.orderInsertedSubject.next();
          this.checkOrderAbilities();
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
  }
}
