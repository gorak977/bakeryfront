import { Component, OnInit } from '@angular/core';
import { MeasureService } from 'src/app/services/measure.service';
import { MeasureRequestService } from 'src/app/services/measure-request.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Measure } from 'src/app/models/measure.interface';

@Component({
  selector: 'app-measure-form',
  templateUrl: './measure-form.component.html',
  styleUrls: ['./measure-form.component.scss']
})
export class MeasureFormComponent implements OnInit {
  createMeasureForm: FormGroup;
  constructor(private measureRequest: MeasureRequestService, public measureService: MeasureService) { }

  ngOnInit() {
    this.createNewForm();

    this.measureService.onUpdateClickSubject.subscribe(result => {
      this.measureService.measureToUpdate = result;
      this.setFormData(result);
    });
  }

  createNewForm() {
    this.createMeasureForm = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      partOfKg: new FormControl(null, [Validators.required, Validators.pattern('^[0-9]*[.,]?[0-9]+$')])
    });
  }

  createMeasure() {
    const measure: Measure = this.getFormData();
    this.measureRequest.setNewMeasure(measure).subscribe(() => this.measureService.measureInsertedSubject.next());
  }

  setFormData(measure: Measure) {
    this.createMeasureForm.setValue({
    name: measure.name,
    partOfKg: measure.partOfKg
    });
  }
  getFormData(): Measure {
    const measure: Measure = {
      idMeasure: 0,
      name: this.createMeasureForm.value.name,
      partOfKg: Number(this.createMeasureForm.value.partOfKg)
    };
    return measure;
  }
  updateButtonClick() {
    const measure = this.getFormData();
    measure.idMeasure = this.measureService.measureToUpdate.idMeasure;

    this.measureRequest.updateMeasure(measure).subscribe(() => {
      this.measureService.measureInsertedSubject.next();
      this.measureService.measureToUpdate = null;
      this.createNewForm();
    });
  }

  deleteButtonClick() {
    this.measureRequest.deleteMeasure(this.measureService.measureToUpdate.idMeasure)
    .subscribe(() => {
      this.measureService.measureInsertedSubject.next();
      this.measureService.measureToUpdate = null;
      this.createNewForm();
    });
  }

  cancelButtonClick() {
    this.measureService.measureToUpdate = null;
    this.createNewForm();
  }
}
