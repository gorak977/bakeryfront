import { Component, OnInit } from '@angular/core';
import { MeasureRequestService } from '../../../services/measure-request.service';
import { MeasureService } from '../../../services/measure.service';

@Component({
  selector: 'app-measures-list',
  templateUrl: './measures-list.component.html',
  styleUrls: ['./measures-list.component.scss']
})
export class MeasuresListComponent implements OnInit {

  constructor(public measureService: MeasureService, private measureRequest: MeasureRequestService) { }

  displayedColumns: string[] = ['number', 'name', 'partOfKg'];

  ngOnInit() {
    this.onLoad();
  }

  onLoad() {
    this.updateList();

    this.measureService.measureInsertedSubject.subscribe(() => this.updateList());
  }

  updateList() {
    this.measureRequest.getAllMeasures().subscribe(result => {
      this.measureService.measureList = result;
    });
  }

  getRow(row) {
    this.measureService.onUpdateClickSubject.next(row);
  }
}
