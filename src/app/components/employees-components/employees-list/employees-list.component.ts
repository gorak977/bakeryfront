import { Component, OnInit } from '@angular/core';
import { EmployeesRequestService } from 'src/app/services/employees-request.service';
import { EmployeesService } from 'src/app/services/employees.service';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.scss']
})
export class EmployeesListComponent implements OnInit {

  constructor(public employeesService: EmployeesService, private employeesRequest: EmployeesRequestService) { }

  displayedColumns: string[] = ['number', 'name', 'surname', 'phoneNumber', 'experience', 'country', 'city', 'street', 'houseNumber', 'position', 'salary'];

  ngOnInit() {
    this.onLoad();
  }

  onLoad() {
    this.updateList();

    this.employeesService.employeeInsertedSubject.subscribe(() => this.updateList());
  }

  updateList() {
    this.employeesRequest.getAllEmployees().subscribe(result => {
      this.employeesService.employeesList = result;
    });
  }

  getRow(employee) {
    this.employeesService.onEmployeeClickSubject.next(employee);
  }
}
