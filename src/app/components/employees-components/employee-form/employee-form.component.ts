import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { EmployeesRequestService } from "src/app/services/employees-request.service";
import { EmployeesService } from "src/app/services/employees.service";
import { Employee } from "src/app/models/employee.interface";
import { PositionService } from "src/app/services/positions.service";
import { PositionRequestService } from "src/app/services/positions-request.service";
import { Position } from "../../../models/position.interface";

@Component({
  selector: "app-employee-form",
  templateUrl: "./employee-form.component.html",
  styleUrls: ["./employee-form.component.scss"],
})
export class EmployeeFormComponent implements OnInit {
  createEmployeeForm: FormGroup;
  constructor(
    private employeesRequest: EmployeesRequestService,
    public employeesService: EmployeesService,
    private positionRequest: PositionRequestService,
    public positionsService: PositionService
  ) {}

  ngOnInit() {
    this.employeesService.employeeToUpdate = null;

    this.createEmployeeForm = new FormGroup({
      name: new FormControl(null, [
        Validators.required,
        Validators.maxLength(20),
      ]),
      surname: new FormControl(null, [
        Validators.required,
        Validators.maxLength(20),
      ]),
      phoneNumber: new FormControl(null, [
        Validators.required,
        Validators.maxLength(13),
      ]),
      experience: new FormControl(null, [Validators.maxLength(20)]),
      country: new FormControl(null, [
        Validators.required,
        Validators.maxLength(20),
      ]),
      city: new FormControl(null, [
        Validators.required,
        Validators.maxLength(20),
      ]),
      street: new FormControl(null, [
        Validators.required,
        Validators.maxLength(20),
      ]),
      houseNumber: new FormControl(null, Validators.required),
      position: new FormControl(null, Validators.required),
    });

    this.employeesService.onEmployeeClickSubject.subscribe((employee) => {
      this.setFormData(employee);
      this.employeesService.employeeToUpdate = employee;
    });
    this.onLoad();
  }

  onLoad() {
    this.updatePositions();
  }

  updatePositions() {
    this.positionRequest
      .getAllPositions()
      .subscribe((positions: Position[]) => {
        this.positionsService.positionList = positions;
      });
  }

  createCustomer() {
    const employee: Employee = this.getFormData();
    this.employeesRequest
      .setNewEmployee(employee)
      .subscribe((result) =>
        this.employeesService.employeeInsertedSubject.next()
      );
    console.log(employee);
  }

  getFormData(): Employee {
    const employee: Employee = {
      idEmployee: 0,
      name: this.createEmployeeForm.value.name,
      surname: this.createEmployeeForm.value.surname,
      phoneNumber: this.createEmployeeForm.value.phoneNumber,
      experience: this.createEmployeeForm.value.experience,
      idAddress: 0,
      address: {
        idAddress: 0,
        country: this.createEmployeeForm.value.country,
        city: this.createEmployeeForm.value.city,
        street: this.createEmployeeForm.value.street,
        houseNumber: Number(this.createEmployeeForm.value.houseNumber),
      },
      idPosition: this.createEmployeeForm.value.position,
      position: {
        idPosition: 0,
        positionName: "",
        salary: 0,
      },
    };
    return employee;
  }

  setFormData(employee: Employee) {
    this.createEmployeeForm.setValue({
      name: employee.name,
      surname: employee.surname,
      phoneNumber: employee.phoneNumber,
      experience: employee.experience,
      country: employee.address.country,
      city: employee.address.city,
      street: employee.address.street,
      houseNumber: employee.address.houseNumber,
      position: employee.idPosition,
    });
  }

  cancelButtonClick() {
    if (this.employeesService.employeeToUpdate != null) {
      this.employeesService.employeeToUpdate = null;
    }
    this.createEmployeeForm.reset();
  }

  updateButtonClick() {
    const employee = this.getFormData();
    employee.idEmployee = this.employeesService.employeeToUpdate.idEmployee;
    employee.idAddress = this.employeesService.employeeToUpdate.idAddress;
    employee.address.idAddress = this.employeesService.employeeToUpdate.address.idAddress;

    this.employeesRequest.updateEmployee(employee).subscribe((result) => {
      this.employeesService.employeeInsertedSubject.next();
      this.employeesService.employeeToUpdate = null;
      this.createEmployeeForm.reset();
    });
  }

  deleteButtonClick() {
    this.employeesRequest
      .deleteEmployee(this.employeesService.employeeToUpdate.idEmployee)
      .subscribe((result) => {
        this.employeesService.employeeInsertedSubject.next();
        this.employeesService.employeeToUpdate = null;
        this.createEmployeeForm.reset();
      });
  }
}
