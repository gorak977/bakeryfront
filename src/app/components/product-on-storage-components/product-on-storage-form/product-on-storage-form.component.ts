import { Component, OnInit } from '@angular/core';
import { ProductOnStorageRequestService } from 'src/app/services/product-on-storage-request.service';
import { ProductOnStorageService } from 'src/app/services/product-on-storage.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductOnStorage } from 'src/app/models/productOnStorage.interface';
import { MatDialog } from '@angular/material';
import { ProductOnStorageCreateComponent } from '../product-on-storage-create/product-on-storage-create.component';

@Component({
  selector: 'app-product-on-storage-form',
  templateUrl: './product-on-storage-form.component.html',
  styleUrls: ['./product-on-storage-form.component.scss']
})
export class ProductOnStorageFormComponent implements OnInit {

  createProductOnStorageForm: FormGroup;
  constructor(private productOnStorageRequest: ProductOnStorageRequestService,
              public productOnStorageService: ProductOnStorageService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.createForm();
    this.productOnStorageService.productInsertedSubject.subscribe(() => this.createForm());
  }
  createForm() {
    this.createProductOnStorageForm = new FormGroup({
      productName: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      productId: new FormControl(null),
      amount: new FormControl(null, Validators.required),
      storageId: new FormControl(null, Validators.required)
    });

    this.createProductOnStorageForm.get('productName').valueChanges.subscribe(name =>
      this.getAutocompleteProductsArray(name)
      );

    this.productOnStorageRequest.getAllStorages().subscribe(result => {
      this.productOnStorageService.storageSelect = result;
      console.log(this.productOnStorageService.storageSelect);
    });
  }
  getAutocompleteProductsArray(name: string) {
    if (name !== '') {
    this.productOnStorageRequest.getProductAutocomplete(name).subscribe(result => {
        this.productOnStorageService.productAutocomplete = result;
      });
    }
  }
  autocompleteProductClick(index) {
    this.createProductOnStorageForm.patchValue({
      productId: this.productOnStorageService.productAutocomplete[index].idProduct
    });
    this.productOnStorageService.productAutocomplete = [];
  }
  createProductOnStorage() {
    const product: ProductOnStorage = {
      idProductOnStorage: 0,
      idProduct: this.createProductOnStorageForm.value.productId,
      idStorage: this.createProductOnStorageForm.value.storageId,
      amount: this.createProductOnStorageForm.value.amount,
      storageAddress: null,
      product: null,
      dateOfManufacture: null
    };
    this.openDialog(product);
  }
  cancelButtonClick() {
    this.createForm();
  }
  openDialog(row: ProductOnStorage ): void {
    const dialogRef = this.dialog.open(ProductOnStorageCreateComponent, {
      data: row
    });
  }
}
