import { Component, OnInit } from '@angular/core';
import { ProductOnStorageRequestService } from 'src/app/services/product-on-storage-request.service';
import { ProductOnStorageService } from 'src/app/services/product-on-storage.service';

@Component({
  selector: 'app-removed-products-list',
  templateUrl: './removed-products-list.component.html',
  styleUrls: ['./removed-products-list.component.scss']
})
export class RemovedProductsListComponent implements OnInit {

  constructor(
    private productOnStorageRequest: ProductOnStorageRequestService,
    public productOnStorageService: ProductOnStorageService) { }

    displayedColumns: string[] = ['number', 'productName', 'amount', 'reasonToRemove', 'removingDate', 'removedBy'];

  ngOnInit() {
    this.updateProducts();
    this.productOnStorageService.productRemovedSubject.subscribe(() => this.updateProducts());
  }
  updateProducts() {
    this.productOnStorageRequest.getAllRemovedProduct().subscribe(result => {
      this.productOnStorageService.removedProductsList = result;
     });
  }
}
