import { Component, OnInit, Inject } from '@angular/core';
import { ProductOnStorage } from 'src/app/models/productOnStorage.interface';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ProductOnStorageService } from 'src/app/services/product-on-storage.service';
import { ProductOnStorageRequestService } from 'src/app/services/product-on-storage-request.service';
import { ProductOnStorageDeleteComponent } from '../product-on-storage-delete/product-on-storage-delete.component';

@Component({
  selector: 'app-product-on-storage-detail',
  templateUrl: './product-on-storage-detail.component.html',
  styleUrls: ['./product-on-storage-detail.component.scss']
})
export class ProductOnStorageDetailComponent implements OnInit {

  product: ProductOnStorage;
  constructor(
    public dialogRef: MatDialogRef<ProductOnStorageDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ProductOnStorage,
    public productOnStorageService: ProductOnStorageService,
    public productOnStorageRequest: ProductOnStorageRequestService,
    public dialog: MatDialog) {
      this.product = data;
  }

  ngOnInit(): void {
  }
  deleteButtonClick() {
    this.dialogRef.close();
    this.openDialog();
  }
  onCloseClick() {
    this.dialogRef.close();
  }

  openDialog( ): void {
    const dialogRef = this.dialog.open(ProductOnStorageDeleteComponent, {
      data: this.product
    });
  }
}
