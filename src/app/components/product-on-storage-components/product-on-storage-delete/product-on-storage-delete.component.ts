import { Component, OnInit, Inject } from '@angular/core';
import { ProductOnStorage } from 'src/app/models/productOnStorage.interface';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProductOnStorageDetailComponent } from '../product-on-storage-detail/product-on-storage-detail.component';
import { ProductOnStorageService } from 'src/app/services/product-on-storage.service';
import { ProductOnStorageRequestService } from 'src/app/services/product-on-storage-request.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EmployeesRequestService } from 'src/app/services/employees-request.service';
import { RemovedProduct } from 'src/app/models/removedProduct.interface';

@Component({
  selector: 'app-product-on-storage-delete',
  templateUrl: './product-on-storage-delete.component.html',
  styleUrls: ['./product-on-storage-delete.component.scss']
})
export class ProductOnStorageDeleteComponent implements OnInit {

  product: ProductOnStorage;
  isProductNotEnough = false;
  deleteProductOnStorageForm: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<ProductOnStorageDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ProductOnStorage,
    public productOnStorageService: ProductOnStorageService,
    public productOnStorageRequest: ProductOnStorageRequestService,
    private employeeRequestService: EmployeesRequestService) {
      this.product = data;
  }
    ngOnInit() {
      this.createNewForm();
      this.employeeRequestService.getAllEmployees().subscribe(result =>
        this.productOnStorageService.employeeSelect = result);

  }

  createNewForm() {
    this.deleteProductOnStorageForm = new FormGroup({
      reasonToRemove: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      employeeId: new FormControl(null),
      amount: new FormControl(null, Validators.required)
    });

    this.deleteProductOnStorageForm.get('amount').valueChanges.subscribe(value => {
      if (value > this.product.amount) {
        this.isProductNotEnough = true;
      } else {
        this.isProductNotEnough = false;
      }
    });
  }
  removeProductOnStorage() {
      const productToRemove: RemovedProduct = {
        idRemovedProduct: 0,
        idEmployee: this.deleteProductOnStorageForm.value.employeeId,
        idProduct: this.product.idProduct,
        amount: this.deleteProductOnStorageForm.value.amount,
        reasonToRemove: this.deleteProductOnStorageForm.value.reasonToRemove,
        removingDate: null,
        product: null,
        employee: null
      };

      this.productOnStorageRequest.removeProductFromStorage(productToRemove, this.product.idProductOnStorage)
      .subscribe(() => this.productOnStorageService.productInsertedSubject.next());

      this.dialogRef.close();
  }

  cancelButtonClick() {
    this.dialogRef.close();
  }

}
