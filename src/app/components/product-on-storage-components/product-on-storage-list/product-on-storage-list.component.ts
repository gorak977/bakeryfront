import { Component, OnInit } from '@angular/core';
import { ProductOnStorageRequestService } from 'src/app/services/product-on-storage-request.service';
import { ProductOnStorageService } from 'src/app/services/product-on-storage.service';
import { ProductOnStorage } from 'src/app/models/productOnStorage.interface';
import { MatDialog } from '@angular/material';
import { ProductOnStorageDetailComponent } from '../product-on-storage-detail/product-on-storage-detail.component';

@Component({
  selector: 'app-product-on-storage-list',
  templateUrl: './product-on-storage-list.component.html',
  styleUrls: ['./product-on-storage-list.component.scss']
})
export class ProductOnStorageListComponent implements OnInit {

  constructor(
    private productOnStorageRequest: ProductOnStorageRequestService,
    public productOnStorageService: ProductOnStorageService,
    public dialog: MatDialog) { }

    displayedColumns: string[] = ['number', 'productName', 'weight', 'measure', 'price'];
  ngOnInit() {
    this.updateProducts();
    this.productOnStorageService.productInsertedSubject.subscribe(() => this.updateProducts());
  }

  updateProducts() {
    this.productOnStorageRequest.getAllProductsOnStorage().subscribe(result => {
      this.productOnStorageService.productOnStorageList = result;
     });
  }

  getRow(row) {
    this.openDialog(row);
  }

  openDialog(row: ProductOnStorage ): void {
    const dialogRef = this.dialog.open(ProductOnStorageDetailComponent, {
      data: row
    });
  }
}
