import { Component, OnInit, Inject } from '@angular/core';
import { ProductOnStorage } from 'src/app/models/productOnStorage.interface';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProductOnStorageDetailComponent } from '../product-on-storage-detail/product-on-storage-detail.component';
import { ProductOnStorageService } from 'src/app/services/product-on-storage.service';
import { ProductOnStorageRequestService } from 'src/app/services/product-on-storage-request.service';
import { IncertingProductOnStorage } from 'src/app/models/insertingProductOnStorage.interface';

@Component({
  selector: 'app-product-on-storage-create',
  templateUrl: './product-on-storage-create.component.html',
  styleUrls: ['./product-on-storage-create.component.scss']
})
export class ProductOnStorageCreateComponent implements OnInit {

  product: ProductOnStorage;
  minAmount = 0;
  createString = 'Create';
  constructor(
    public dialogRef: MatDialogRef<ProductOnStorageDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ProductOnStorage,
    public productOnStorageService: ProductOnStorageService,
    public productOnStorageRequest: ProductOnStorageRequestService) {
      this.product = data;
  }
  displayedColumns: string[] = ['number', 'rawStaffName', 'neededAmount', 'amountOnStorage', 'measure'];

  ngOnInit(): void {
    this.productOnStorageRequest.getNeededRawStaff(this.product).subscribe(result => {
      this.productOnStorageService.neededRawStaffList = result;
      if (this.productOnStorageService.neededRawStaffList !== []) {
      this.minAmount = this.productOnStorageService.neededRawStaffList[0].quantityToManufacture;
      this.productOnStorageService.neededRawStaffList.forEach(raw => {
        if (raw.quantityToManufacture < this.minAmount) {
          this.minAmount = raw.quantityToManufacture;
        }
      });
    }
      if (this.minAmount > 0 && this.minAmount < this.product.amount) {
      this.createString += ' ' + this.minAmount;
      }
    });
  }
  createButtonClick() {
      if (this.minAmount < this.product.amount) {
        this.product.amount = this.minAmount;
      }
      const prod: IncertingProductOnStorage = {
        product: this.product,
        employeesId: []
      };
      this.productOnStorageRequest.setNewProduct(prod)
      .subscribe(() => this.productOnStorageService.productInsertedSubject.next());
      this.dialogRef.close();
  }
  onCloseClick() {
    this.dialogRef.close();
  }
}
