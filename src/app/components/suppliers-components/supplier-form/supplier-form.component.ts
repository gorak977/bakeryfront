import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators, FormArray } from "@angular/forms";
import { Supplier } from "src/app/models/supplier.interface";
import { SuppliersRequestService } from "src/app/services/suppliers-request.service";
import { SuppliersService } from "src/app/services/suppliers.service";
import { RawStaff } from "src/app/models/rawStaff.interface";

@Component({
  selector: "app-supplier-form",
  templateUrl: "./supplier-form.component.html",
  styleUrls: ["./supplier-form.component.scss"],
})
export class SupplierFormComponent implements OnInit {
  createSupplierForm: FormGroup;
  constructor(
    private suppliersRequest: SuppliersRequestService,
    public suppliersService: SuppliersService
  ) {}

  ngOnInit() {
    this.suppliersService.supplierToUpdate = null;

    this.createSupplierForm = new FormGroup({
      supplierName: new FormControl(null, [
        Validators.required,
        Validators.maxLength(20),
      ]),
      phoneNumber: new FormControl(null, [
        Validators.required,
        Validators.maxLength(13),
      ]),
      country: new FormControl(null, [
        Validators.required,
        Validators.maxLength(20),
      ]),
      city: new FormControl(null, [
        Validators.required,
        Validators.maxLength(20),
      ]),
      street: new FormControl(null, [
        Validators.required,
        Validators.maxLength(20),
      ]),
      houseNumber: new FormControl(null, Validators.required),
      rawStaffArray: new FormControl([], Validators.required),
      rawStaffList: new FormArray([]),
    });
    this.onLoad();

    this.suppliersService.onSupplierClickSubject.subscribe((supplier) => {
      this.suppliersService.supplierToUpdate = supplier;
      this.setSupplierDataToForm(supplier);
      this.onLoad();
    });
  }

  onLoad() {
    this.updateRawStaff();

    this.suppliersService.supplierInsertedSubject.subscribe(() =>
      this.updateRawStaff()
    );
  }

  updateRawStaff() {
    this.suppliersService.supplierRawStaffList = null;
    if (this.suppliersService.supplierToUpdate != null) {
      this.suppliersRequest
        .getAllSupplierRawStaff(
          this.suppliersService.supplierToUpdate.idSupplier
        )
        .subscribe((result) => {
          this.suppliersService.supplierRawStaffList = result;
          this.setRawStaffArrayToForm(
            this.suppliersService.supplierRawStaffList
          );
        });
    }
    this.suppliersRequest.getAllRawStaff().subscribe((result) => {
      this.suppliersService.rawStaffList = result;
    });
  }

  getFormData(): Supplier {
    const supplier: Supplier = {
      idSupplier: 0,
      name: this.createSupplierForm.value.supplierName,
      phoneNumber: this.createSupplierForm.value.phoneNumber,
      idAddress: 0,
      address: {
        idAddress: 0,
        country: this.createSupplierForm.value.country,
        city: this.createSupplierForm.value.city,
        street: this.createSupplierForm.value.street,
        houseNumber: Number(this.createSupplierForm.value.houseNumber),
      },
    };
    return supplier;
  }

  setSupplierDataToForm(supplier: Supplier) {
    this.createSupplierForm.patchValue({
      supplierName: supplier.name,
      phoneNumber: supplier.phoneNumber,
      country: supplier.address.country,
      city: supplier.address.city,
      street: supplier.address.street,
      houseNumber: supplier.address.houseNumber,
    });
  }

  setRawStaffArrayToForm(rawStaff: RawStaff[]) {
    const rawStaffIDs: number[] = [];
    rawStaff.forEach((el) => {
      rawStaffIDs.push(el.idRawStaff);
    });
    this.createSupplierForm.patchValue({
      rawStaffArray: rawStaffIDs,
    });
  }

  cancelButtonClick() {
    if (this.suppliersService.supplierToUpdate != null) {
      this.suppliersService.supplierToUpdate = null;
    }
    this.createSupplierForm.reset();
  }

  createSupplier() {
    const supplier: Supplier = this.getFormData();
    const rawStaffIDs: number[] = this.createSupplierForm.value.rawStaffArray;

    this.suppliersRequest.setNewSupplier(supplier).subscribe((result) => {
      this.suppliersService.supplierInsertedSubject.next();
      this.suppliersRequest
        .setSupplierRawStaff(rawStaffIDs, result.idSupplier)
        .subscribe(() => {
          this.suppliersService.supplierToUpdate = null;
          this.createSupplierForm.reset();
        });
    });
  }

  updateButtonClick() {
    const supplier = this.getFormData();
    const rawStaffIDs: number[] = this.createSupplierForm.value.rawStaffArray;
    supplier.idSupplier = this.suppliersService.supplierToUpdate.idSupplier;
    supplier.idAddress = this.suppliersService.supplierToUpdate.idAddress;
    supplier.address.idAddress = this.suppliersService.supplierToUpdate.address.idAddress;

    this.suppliersRequest.updateSupplier(supplier).subscribe((result) => {
      this.suppliersService.supplierInsertedSubject.next();
      this.suppliersRequest
        .setSupplierRawStaff(rawStaffIDs, supplier.idSupplier)
        .subscribe(() => {
          this.suppliersService.supplierToUpdate = null;
          this.createSupplierForm.reset();
        });
    });
  }

  deleteButtonClick() {
    this.suppliersRequest
      .deleteSupplier(this.suppliersService.supplierToUpdate.idSupplier)
      .subscribe((result) => {
        this.suppliersService.supplierInsertedSubject.next();
        this.suppliersService.supplierToUpdate = null;
        this.createSupplierForm.reset();
      });
  }
}
