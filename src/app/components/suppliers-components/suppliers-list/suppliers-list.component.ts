import { Component, OnInit, ViewChild } from '@angular/core';
import { SuppliersService } from 'src/app/services/suppliers.service';
import { SuppliersRequestService } from 'src/app/services/suppliers-request.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Customer } from 'src/app/models/customer.interface';

@Component({
  selector: 'app-suppliers-list',
  templateUrl: './suppliers-list.component.html',
  styleUrls: ['./suppliers-list.component.scss']
})
export class SuppliersListComponent implements OnInit {

  constructor(public suppliersService: SuppliersService, private suppliersRequest: SuppliersRequestService) { }

  displayedColumns: string[] = ['number', 'supplierName', 'phoneNumber', 'country', 'city', 'street', 'houseNumber'];

  ngOnInit() {
    this.onLoad();
  }

  onLoad() {
    this.updateList();

    this.suppliersService.supplierInsertedSubject.subscribe(() => this.updateList());
  }

  updateList() {
    this.suppliersRequest.getAllSuppliers().subscribe(result => {
      this.suppliersService.suppliersList = result;
    });
  }

  getRow(supplier) {
    this.suppliersService.onSupplierClickSubject.next(supplier);
  }
}
