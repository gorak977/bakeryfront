import { Component, OnInit, ViewChild } from '@angular/core';
import { CustomersService } from 'src/app/services/customers.service';
import { CustomersRequestService } from 'src/app/services/customers-request.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Customer } from 'src/app/models/customer.interface';

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.scss']
})
export class CustomersListComponent implements OnInit {

  constructor(public customersService: CustomersService, private customersRequest: CustomersRequestService) { }

  displayedColumns: string[] = ['number', 'customerName', 'phoneNumber', 'country', 'city', 'street', 'houseNumber'];

  ngOnInit() {
    this.onLoad();
  }

  onLoad() {
    this.updateList();

    this.customersService.customerInsertedSubject.subscribe(() => this.updateList());
  }

  updateList() {
    this.customersRequest.getAllCustomers().subscribe(result => {
      this.customersService.customersList = result;
    });
  }

  getRow(customer) {
    this.customersService.onCustomerClickSubject.next(customer);
  }
}
