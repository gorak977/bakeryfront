import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Customer } from 'src/app/models/customer.interface';
import { CustomersRequestService } from 'src/app/services/customers-request.service';
import { CustomersService } from 'src/app/services/customers.service';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.scss']
})
export class CustomerFormComponent implements OnInit {
  createCustomerForm: FormGroup;
  constructor(private customersRequest: CustomersRequestService,
              private customersService: CustomersService) { }

  ngOnInit() {
    this.createCustomerForm = new FormGroup({
      customerName: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      phoneNumber: new FormControl(null, [Validators.required, Validators.maxLength(13)]),
      country: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      city: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      street: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      houseNumber: new FormControl(null, Validators.required),
    });

    this.customersService.onCustomerClickSubject.subscribe(customer => {
        this.setFormData(customer);
        this.customersService.customerToUpdate = customer;
    });
  }

  createCustomer() {
    const customer: Customer = this.getFormData();
    this.customersRequest.setNewCustomer(customer).subscribe(
      result => this.customersService.customerInsertedSubject.next()
    );
    console.log(customer);
  }

  getFormData(): Customer {
    const customer: Customer = {
      idCustomer: 0,
      name: this.createCustomerForm.value.customerName,
      phoneNumber: this.createCustomerForm.value.phoneNumber,
      idAddress: 0,
      address: {
        idAddress: 0,
        country: this.createCustomerForm.value.country,
        city: this.createCustomerForm.value.city,
        street: this.createCustomerForm.value.street,
        houseNumber: Number(this.createCustomerForm.value.houseNumber)
      }
    };
    return customer;
  }

  setFormData(customer: Customer) {
    this.createCustomerForm.setValue({
    customerName: customer.name,
    phoneNumber: customer.phoneNumber,
    country: customer.address.country,
    city: customer.address.city,
    street: customer.address.street,
    houseNumber: customer.address.houseNumber
    });
  }

  cancelButtonClick() {
    if (this.customersService.customerToUpdate != null) {
      this.customersService.customerToUpdate = null;
    }
    this.createCustomerForm.reset();
  }

  updateButtonClick() {
    const customer = this.getFormData();
    customer.idCustomer = this.customersService.customerToUpdate.idCustomer;
    customer.idAddress = this.customersService.customerToUpdate.idAddress;
    customer.address.idAddress = this.customersService.customerToUpdate.address.idAddress;

    this.customersRequest.updateCustomer(customer).subscribe(result => {
        this.customersService.customerInsertedSubject.next();
        this.customersService.customerToUpdate = null;
        this.createCustomerForm.reset();
      });
  }

  deleteButtonClick() {
    this.customersRequest.deleteCustomer(this.customersService.customerToUpdate.idCustomer)
    .subscribe(result => {
      this.customersService.customerInsertedSubject.next();
      this.customersService.customerToUpdate = null;
      this.createCustomerForm.reset();
    });
  }

}
