import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Product } from 'src/app/models/product.interface';
import { Ingredient } from 'src/app/models/ingredient.interface';
import { ProductsRequestService } from 'src/app/services/products-request.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-products-form',
  templateUrl: './products-form.component.html',
  styleUrls: ['./products-form.component.scss']
})
export class ProductsFormComponent implements OnInit {
  createProductForm: FormGroup;
  errorIn = -1;
  constructor(private productRequest: ProductsRequestService, public productService: ProductService) { }

  ngOnInit() {
    this.createNewForm();

    this.productService.onUpdateClickSubject.subscribe(result => {
      this.productService.productToUpdate = result;
      this.setProductToForm(result);
      this.setIngredientsToForm(this.productService.ingredientsList);
    });
  }

  createNewForm() {
    this.errorIn = -1;
    this.createProductForm = new FormGroup({
      productName: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      weight: new FormControl(null, [Validators.required, Validators.pattern('^[0-9]*[.,]?[0-9]+$')]),
      description: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      expirationTerm: new FormControl(null, [Validators.required, Validators.pattern('^[0-9]*')]),
      markUp: new FormControl(null, [Validators.required, Validators.pattern('^[0-9]*[.,]?[0-9]+$')]),
      idMeasure: new FormControl(null, Validators.required),
      ingredients: new FormArray([])
    });

    this.productRequest.getAllMeasure().subscribe(result => this.productService.measureSelect = result);
  }
  getAutocompleteMeasureArray(name: string) {
    this.productRequest.getMeasureAutocomplete(name).subscribe(result => {
        this.productService.measureAutocomplete = result;
        this.productService.measureNamesAutocomplete = this.productService
          .getNames(this.productService.measureAutocomplete);
      });
  }
  getAutocompleteRawStaffArray(name: string) {
    this.productRequest.getRawStaffAutocomplete(name).subscribe(result => {
      this.productService.rawStaffAutocomplete = result;
      this.productService.rawStaffNamesAutocomplete = this.productService
      .getNames(this.productService.rawStaffAutocomplete);
    });
  }
  addIngredient() {
    this.errorIn = -1;
    const control = new FormGroup({
      rawStaff: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      weight: new FormControl(null, [Validators.required, Validators.pattern('^[0-9]*[.,]?[0-9]+$')]),
      idMeasure: new FormControl(null, Validators.required),
      idRawStaff: new FormControl(null)
    });
    control.get('rawStaff').valueChanges.subscribe(name => {
      this.errorIn = -1;
      if (name !== '') {
      this.getAutocompleteRawStaffArray(name);
      }
    });
    (this.createProductForm.get('ingredients') as FormArray).push(control);
  }

  deleteIngredient(index: number) {
    this.errorIn = -1;
    (this.createProductForm.get('ingredients') as FormArray).removeAt(index);
  }

  getProductFromForm(): Product {
    const product: Product = {
      idProduct: 0,
      productName: this.createProductForm.value.productName,
      weight: Number(this.createProductForm.value.weight),
      desctiption: this.createProductForm.value.description,
      expirationTerm: Number(this.createProductForm.value.expirationTerm),
      markUp: Number(this.createProductForm.value.markUp),
      measureName: '',
      idMeasure: Number(this.createProductForm.value.idMeasure),
      price: 0
    };
    return product;
  }

  setProductToForm(product: Product) {
    this.createProductForm.patchValue({
      productName: product.productName,
      weight: product.weight,
      description: product.desctiption,
      expirationTerm: product.expirationTerm,
      markUp: product.markUp,
      idMeasure: product.idMeasure
    });
  }

  setIngredientsToForm(ingredients: Ingredient[]) {
    (this.createProductForm.get('ingredients') as FormArray).clear();
    let i = 0;
    ingredients.forEach(ing => {
      this.addIngredient();
      (this.createProductForm.get('ingredients') as FormArray).controls[i].patchValue({
        rawStaff: ingredients[i].rawStaffName,
        weight: ingredients[i].weight,
        idMeasure: ingredients[i].idMeasure,
        idRawStaff: ingredients[i].idRawStaff
      });
      i++;
    });
  }

  getIndredientsFromForm(): Ingredient[] {
    const ingredients: Ingredient[] = [];
    this.createProductForm.value.ingredients.forEach(element => {
      const ingredient: Ingredient = {
        idIngredient: 0,
        idMeasure: Number(element.idMeasure),
        idProduct: null,
        idRawStaff: Number(element.idRawStaff),
        weight: Number(element.weight),
        measureName: '',
        productName: this.createProductForm.value.productName,
        rawStaffName: element.rawStaff
      };
      ingredients.push(ingredient);
    });
    return ingredients;
  }

  createProduct() {
    this.errorIn = -1;
    const product = this.getProductFromForm();
    const ingredients = this.getIndredientsFromForm();
    this.productRequest.setNewProduct(product).subscribe(result => {
      ingredients.forEach(ing => ing.idProduct = result.idProduct);
      this.productRequest.setNewIngredients(ingredients).subscribe( () => {
        this.productService.productInsertedSubject.next();
        this.createNewForm();
      }, error => {
        this.productRequest.deleteProduct(result.idProduct).subscribe();
        this.errorIn = Number(error.error);
      });
    });
  }

  autocompleteMeasureClick(option) {
    this.createProductForm.patchValue({
      idMeasure: this.productService.measureAutocomplete[option].idMeasure
    });
    this.productService.measureAutocomplete = [];
    this.productService.measureNamesAutocomplete = [];
  }

  autocompleteArrayMeasureClick(arrayIndex, autocompleteIndex) {
    (this.createProductForm.get('ingredients') as FormArray).controls[arrayIndex].patchValue({
      idMeasure: this.productService.measureAutocomplete[autocompleteIndex].idMeasure
    });
    this.productService.measureAutocomplete = [];
    this.productService.measureNamesAutocomplete = [];
  }
  autocompleteArrayRawStaffClick(arrayIndex, autocompleteIndex) {
    (this.createProductForm.get('ingredients') as FormArray).controls[arrayIndex].patchValue({
      idRawStaff: this.productService.rawStaffAutocomplete[autocompleteIndex].idRawStaff
    });
    this.productService.rawStaffAutocomplete = [];
    this.productService.rawStaffNamesAutocomplete = [];
  }

  cancelButtonClick() {
    this.createNewForm();
    this.productService.ingredientsList = [];
    this.productService.productToUpdate = null;
  }

  updateButtonClick() {
    const product = this.getProductFromForm();
    product.idProduct = this.productService.productToUpdate.idProduct;
    const ingredients = this.getIndredientsFromForm();
    let i = 0;
    ingredients.forEach(ing => {
      if (i <= this.productService.ingredientsList.length - 1) {
      ing.idIngredient = this.productService.ingredientsList[i].idIngredient;
      } else {
        ing.idIngredient = 0;
      }
      ing.idProduct = this.productService.productToUpdate.idProduct;
      i++;
    });
    this.productRequest.updateProduct(product).subscribe(result => {
      this.productRequest.updateIngredientsRange(ingredients)
      .subscribe( () => this.productService.productInsertedSubject.next());
    });
    this.productService.productToUpdate = null;
    this.productService.ingredientsList = [];
    this.createNewForm();
  }
}
