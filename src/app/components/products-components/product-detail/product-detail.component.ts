import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Product } from 'src/app/models/product.interface';
import { ProductService } from 'src/app/services/product.service';
import { ProductsRequestService } from 'src/app/services/products-request.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent {
 product: Product;
  constructor(
    public dialogRef: MatDialogRef<ProductDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Product,
    public productService: ProductService,
    public productRequestService: ProductsRequestService) {
      this.product = data;
    }

    displayedColumns: string[] = ['number', 'rawStaffName', 'weight', 'measure'];

  onCloseClick(): void {
    this.productService.ingredientsList = [];
    this.dialogRef.close();
  }
  updateButtonClick() {
    this.productService.onUpdateClickSubject.next(this.product);
    this.dialogRef.close();
  }
  deleteButtonClick() {
    this.productRequestService.deleteProduct(this.product.idProduct)
      .subscribe(() => this.productService.productInsertedSubject.next());
    this.productService.ingredientsList = [];
    this.dialogRef.close();
  }

}
