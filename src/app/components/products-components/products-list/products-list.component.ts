import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import {ProductsRequestService} from 'src/app/services/products-request.service';
import { ProductService } from 'src/app/services/product.service';
import { ProductDetailComponent } from '../product-detail/product-detail.component';
import { Product } from 'src/app/models/product.interface';


@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

  constructor(public productService: ProductService, public dialog: MatDialog, private productRequest: ProductsRequestService) { }

  displayedColumns: string[] = ['number', 'productName', 'weight', 'measure', 'price'];

  ngOnInit() {
    this.updateProducts();
    this.productService.productInsertedSubject.subscribe(() => this.updateProducts());
  }

  updateProducts() {
    this.productRequest.getAllProducts().subscribe(result => {
      this.productService.productList = result;
     });
  }

  getRow(row: Product) {
    this.productRequest.getIngredientsByProductId(row.idProduct).subscribe(result => {
      this.productService.ingredientsList = result;
      this.openDialog(row);
    });
  }

  openDialog(row: Product ): void {
    const dialogRef = this.dialog.open(ProductDetailComponent, {
      data: row
    });
  }

}
