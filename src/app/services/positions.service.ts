import { Injectable } from '@angular/core';
import { Position } from '../models/position.interface';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PositionService {

  constructor() { }

  public positionList: Position[] = [];
  public positionToUpdate: Position = null;

  positionInsertedSubject = new Subject();
  onUpdateClickSubject = new Subject<Position>();
}
