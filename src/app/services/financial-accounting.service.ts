import { Injectable } from '@angular/core';
import { Outlay } from '../models/outlay.interface';
import { Incomes } from '../models/incomes.interface';

@Injectable({
  providedIn: 'root'
})
export class FinancialAccountingService {

  constructor() { }

  public outlay: Outlay[] = [];
  public incomes: Incomes[] = []

}
