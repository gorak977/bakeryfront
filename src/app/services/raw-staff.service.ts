import { Injectable } from '@angular/core';
import { RawStaff } from '../models/rawStaff.interface';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RawStaffService {

  constructor() { }

  public rawStaffList: RawStaff[] = [];
  public rawStaffToUpdate: RawStaff = null;

  rawStaffInsertedSubject = new Subject();
  onUpdateClickSubject = new Subject<RawStaff>();
}
