import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import { RawStaffOrder } from "../models/rawStaffOrder.interface";
import { RawStaffOrderItem } from "../models/rawStaffOrderItem.interface";

@Injectable({
  providedIn: "root",
})
export class RawStaffOrdersRequestService {
  private readonly apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) {}

  getAllOrders(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/RawStaffOrder`);
  }

  getOrder(orderId: number): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/RawStaffOrder/${orderId}`);
  }

  getOrderItemsByOrderId(orderId: number): Observable<any> {
    return this.httpClient.get(
      `${this.apiUrl}/RawStaffOrder/OrderItem/ByOrder/${orderId}`
    );
  }

  getAllSuppliers(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Supplier`);
  }

  setNewOrder(order: RawStaffOrder): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/RawStaffOrder`, order);
  }

  setNewOrderItems(items: RawStaffOrderItem[]): Observable<any> {
    return this.httpClient.post(
      `${this.apiUrl}/RawStaffOrder/OrderItem/AddRange`,
      items
    );
  }

  updateOrder(order: RawStaffOrder): Observable<any> {
    return this.httpClient.put(`${this.apiUrl}/RawStaffOrder`, order);
  }

  updateOrderItemRange(orderItems: RawStaffOrderItem[]): Observable<any> {
    console.log(orderItems);
    return this.httpClient.put(
      `${this.apiUrl}/RawStaffOrder/OrderItem/UpdateRange`,
      orderItems
    );
  }

  cancelOrder(orderId: number): Observable<any> {
    return this.httpClient.delete(`${this.apiUrl}/RawStaffOrder/${orderId}`);
  }

  confirmOrder(orderID: number, storageID: number): Observable<any> {
    return this.httpClient.post(
      `${this.apiUrl}/RawStaffOrder/Confirm?orderId=${orderID}&storageId=${storageID}`,
      null
    );
  }

  getAllSupplierRawStaff(id: number): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Supplier/${id}/RawStaff`);
  }

  getAllStorages(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/MadeProduct/Storage`);
  }

  checkAbilityToUpdate(idOrder: number): Observable<any> {
    return this.httpClient.get(
      `${this.apiUrl}/RawStaffOrder/CheckAbilityToUpdate/${idOrder}`
    );
  }
  checkAbilityToCancel(idOrder: number): Observable<any> {
    return this.httpClient.get(
      `${this.apiUrl}/RawStaffOrder/CheckAbilityToCancel/${idOrder}`
    );
  }
  checkAbilityToConfirm(idOrder: number): Observable<any> {
    return this.httpClient.get(
      `${this.apiUrl}/RawStaffOrder/CheckAbilityToConfirm/${idOrder}`
    );
  }
}
