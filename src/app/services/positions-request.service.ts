import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import { Position } from '../models/position.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PositionRequestService {
  private readonly apiUrl = environment.apiUrl;
  constructor(private httpClient: HttpClient) { }

  getAllPositions(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Position`);
  }

  setNewPosition(position: Position): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/Position`, position);
  }

  updatePosition(position: Position): Observable<any> {
    return this.httpClient.put(`${this.apiUrl}/Position`, position);
  }
  deletePosition(id: number) {
    return this.httpClient.delete(`${this.apiUrl}/Position/${id}`);
  }
}
