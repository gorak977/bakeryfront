import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Measure } from "../models/measure.interface";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class FinancialAccountingRequestService {
  private readonly apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) {}

  getOutlays(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Outlay`);
  }
  getIncomes(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Incomes`);
  }
}
