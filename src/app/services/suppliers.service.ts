import { Injectable } from '@angular/core';
import { Supplier } from '../models/supplier.interface';
import { Subject } from 'rxjs';
import { RawStaff } from '../models/rawStaff.interface';

@Injectable({
  providedIn: 'root'
})
export class SuppliersService {

  constructor() { }

  public suppliersList: Supplier[] = [];
  public supplierToUpdate: Supplier = null;
  public supplierRawStaffList: RawStaff[] = null;
  public rawStaffList: RawStaff[] = [];

  supplierInsertedSubject = new Subject();
  onSupplierClickSubject = new Subject<Supplier>();
}
