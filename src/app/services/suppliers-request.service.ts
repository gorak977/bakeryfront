import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { environment } from "../../environments/environment";
import { Observable } from "rxjs";
import { Supplier } from "../models/supplier.interface";

@Injectable({
  providedIn: "root",
})
export class SuppliersRequestService {
  private readonly apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) {}

  getAllSuppliers(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Supplier`);
  }
  setNewSupplier(supplier: Supplier): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/Supplier`, supplier);
  }
  setSupplierRawStaff(rawStaffIDs: number[], supplierID: number): Observable<any>
  {
    return this.httpClient.post(`${this.apiUrl}/Supplier/AddRawStaffToSupplier?supplierId=${supplierID}`, rawStaffIDs);
  }
  updateSupplier(supplier: Supplier): Observable<any> {
    return this.httpClient.put(`${this.apiUrl}/Supplier`, supplier);
  }
  deleteSupplier(id: number) {
    return this.httpClient.delete(`${this.apiUrl}/Supplier/${id}`);
  }
  getAllSupplierRawStaff(id: number): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Supplier/${id}/RawStaff`);
  }
  getAllRawStaff(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/RawStaff`);
  }
}
