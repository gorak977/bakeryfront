import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { environment } from "../../environments/environment";
import { Observable } from "rxjs";
import { Employee } from "../models/employee.interface";

@Injectable({
  providedIn: "root",
})
export class EmployeesRequestService {
  private readonly apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) {}

  getAllEmployees(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Employee`);
  }
  setNewEmployee(employee: Employee): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/Employee`, employee);
  }
  updateEmployee(employee: Employee): Observable<any> {
    return this.httpClient.put(`${this.apiUrl}/Employee`, employee);
  }
  deleteEmployee(id: number) {
    return this.httpClient.delete(`${this.apiUrl}/Employee/${id}`);
  }
}
