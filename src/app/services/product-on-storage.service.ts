import { Injectable } from "@angular/core";
import { ProductOnStorage } from "../models/productOnStorage.interface";
import { Product } from "../models/product.interface";
import { Storage } from "../models/storage.interface";
import { Subject } from "rxjs";
import { NeededRawStaff } from "../models/neededRawStaff.interface";
import { Employee } from "../models/employee.interface";
import { RemovedProduct } from "../models/removedProduct.interface";

@Injectable({
  providedIn: "root",
})
export class ProductOnStorageService {
  constructor() {}

  public productOnStorageList: ProductOnStorage[] = [];
  public productAutocomplete: Product[] = [];
  public storageSelect: Storage[] = [];
  public neededRawStaffList: NeededRawStaff[] = [];
  public employeeSelect: Employee[] = [];
  public removedProductsList: RemovedProduct[] = [];

  productInsertedSubject = new Subject();
  productRemovedSubject = new Subject();
}
