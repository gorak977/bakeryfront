import { Injectable } from '@angular/core';
import { Customer } from '../models/customer.interface';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  constructor() { }

  public customersList: Customer[] = [];
  public customerToUpdate: Customer = null;

  customerInsertedSubject = new Subject();
  onCustomerClickSubject = new Subject<Customer>();
}
