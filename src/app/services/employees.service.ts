import { Injectable } from '@angular/core';
import { Employee } from '../models/employee.interface';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  constructor() { }

  public employeesList: Employee[] = [];
  public employeeToUpdate: Employee = null;

  employeeInsertedSubject = new Subject();
  onEmployeeClickSubject = new Subject<Employee>();
}
