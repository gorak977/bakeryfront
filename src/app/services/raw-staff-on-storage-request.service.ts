import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../environments/environment';
import { Observable } from 'rxjs';
import { RawStaffOnStorage } from '../models/rawStaffOnStorage.interface';
import { RemovedRawStaff } from '../models/removedRawStaff.interface';

@Injectable({
  providedIn: 'root'
})
export class RawStaffOnStorageRequestService {

  private readonly apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  getAllRawStaffOnStorage(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/RawStaff/OnStorage`);
  }
  getAllStorages(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/MadeProduct/Storage`);
  }
  removeRawStaffFromStorage(rawStaff: RemovedRawStaff, rawStaffOnStorageId: number): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/RawStaff/RemoveFromStorage?rawStaffOnStorageId=${rawStaffOnStorageId}`, rawStaff);
  }
  getAllRemovedRawStaff(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/RawStaff/Removed`);
  }
}
