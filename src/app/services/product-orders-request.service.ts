import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ProductsOrder } from '../models/productsOrder.interface';
import { ProductOrderItem } from '../models/productOrderItem.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductOrdersRequestService {
  private readonly apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  getAllOrders(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/ProductOrder`);
  }
  getOrderItemsByOrderId(orderId: number): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/ProductOrder/OrderItem/ByOrder/${orderId}`);
  }
  getProductAutocomplete(name: string): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Product/GetByName/${name}`);
  }
  getAllCustomers(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Customer`);
  }

  setNewOrder(order: ProductsOrder): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/ProductOrder`, order);
  }

  setNewOrderItems(items: ProductOrderItem[]): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/ProductOrder/OrderItem/AddRange`, items);
  }

  updateOrder(order: ProductsOrder): Observable<any> {
    return this.httpClient.put(`${this.apiUrl}/ProductOrder`, order);
  }
  updateOrderItemRange(orderItems: ProductOrderItem[]): Observable<any> {
    return this.httpClient.put(`${this.apiUrl}/ProductOrder/OrderItem/UpdateRange`, orderItems);
  }
  cancelOrder(orderId: number): Observable<any> {
    return this.httpClient.delete(`${this.apiUrl}/ProductOrder/${orderId}`);
  }
  getSpecialStatuses(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/SpecialStatuses`);
  }
  confirmOrder(orderId: number): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/ConfirmOrder?orderId=${orderId}`, null);
  }
}
