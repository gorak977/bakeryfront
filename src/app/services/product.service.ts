import { Injectable } from '@angular/core';
import { Product } from '../models/product.interface';
import { Ingredient } from '../models/ingredient.interface';
import { Measure } from '../models/measure.interface';
import { RawStaff } from '../models/rawStaff.interface';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor() { }

  public productList: Product[] = [];
  public ingredientsList: Ingredient[] = [];
  public measureAutocomplete: Measure[] = [];
  public measureSelect: Measure[] = [];
  public measureNamesAutocomplete: string[] = [];
  public rawStaffAutocomplete: RawStaff[] = [];
  public rawStaffNamesAutocomplete: string[] = [];

  public productToUpdate: Product = null;
  productInsertedSubject = new Subject();
  onUpdateClickSubject = new Subject<Product>();


  getNames(measures: any[]): string[] {
    const newArray = measures.map( el => {
      return el.name;
     });
    return newArray;
  }
}
