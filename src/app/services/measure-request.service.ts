import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import { Measure } from '../models/measure.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MeasureRequestService {
  private readonly apiUrl = environment.apiUrl;
  constructor(private httpClient: HttpClient) { }

  getAllMeasures(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Measure`);
  }

  setNewMeasure(measure: Measure): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/Measure`, measure);
  }

  updateMeasure(measure: Measure): Observable<any> {
    return this.httpClient.put(`${this.apiUrl}/Measure`, measure);
  }
  deleteMeasure(id: number) {
    return this.httpClient.delete(`${this.apiUrl}/Measure/${id}`);
  }
}
