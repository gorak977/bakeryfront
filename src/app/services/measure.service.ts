import { Injectable } from '@angular/core';
import { Measure } from '../models/measure.interface';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MeasureService {

  constructor() { }

  public measureList: Measure[] = [];
  public measureToUpdate: Measure = null;

  measureInsertedSubject = new Subject();
  onUpdateClickSubject = new Subject<Measure>();
}
