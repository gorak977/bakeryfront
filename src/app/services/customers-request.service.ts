import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../environments/environment';
import { Observable } from 'rxjs';
import { Customer } from '../models/customer.interface';

@Injectable({
  providedIn: 'root'
})
export class CustomersRequestService {
  private readonly apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) {}

  getAllCustomers(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Customer`);
  }
  setNewCustomer(customer: Customer): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/Customer`, customer);
  }
  updateCustomer(customer: Customer): Observable<any> {
    return this.httpClient.put(`${this.apiUrl}/Customer`, customer);
  }
  deleteCustomer(id: number) {
    return this.httpClient.delete(`${this.apiUrl}/Customer/${id}`);
  }
}
