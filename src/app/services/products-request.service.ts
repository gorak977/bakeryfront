import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../environments/environment';
import { Observable } from 'rxjs';
import { Product } from '../models/product.interface';
import { Ingredient } from '../models/ingredient.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductsRequestService {
  private readonly apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  getAllProducts(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Product`);
  }

  getIngredientsByProductId(productId: number): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Product/Ingredient/ByProduct${productId}`);
  }

  setNewProduct(product: Product): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/Product`, product);
  }

  setNewIngredients(ingredients: Ingredient[]): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/Product/Ingredient/AddRange`, ingredients);
  }

  getMeasureAutocomplete(name: string): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Measure/${name}`);
  }

  getAllMeasure(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Measure`);
  }

  getRawStaffAutocomplete(name: string): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/RawStaff/GetByName/${name}`);
  }

  updateProduct(product: Product): Observable<any> {
    return this.httpClient.put(`${this.apiUrl}/Product`, product);
  }
  updateIngredientsRange(ingredients: Ingredient[]): Observable<any> {
    return this.httpClient.put(`${this.apiUrl}/Product/Ingredient/UpdateRange`, ingredients);
  }
  deleteProduct(productId: number): Observable<any> {
    return this.httpClient.delete(`${this.apiUrl}/Product/` + productId);
  }
}
