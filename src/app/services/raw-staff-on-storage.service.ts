import { Injectable } from "@angular/core";
import { RawStaffOnStorage } from "../models/rawStaffOnStorage.interface";
import { RawStaff } from "../models/rawStaff.interface";
import { Storage } from "../models/storage.interface";
import { Subject } from "rxjs";
import { Employee } from "../models/employee.interface";
import { RemovedRawStaff } from "../models/removedRawStaff.interface";

@Injectable({
  providedIn: "root",
})
export class RawStaffOnStorageService {
  constructor() {}

  public rawStaffOnStorageList: RawStaffOnStorage[] = [];
  public storagesList: Storage[] = [];
  public employeeSelect: Employee[] = [];
  public removedRawStaffList: RemovedRawStaff[] = [];

  rawStaffInsertedSubject = new Subject();
  rawStaffRemovedSubject = new Subject();
}
