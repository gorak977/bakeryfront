import { Injectable } from '@angular/core';
import { User, UserRole } from '../models/user.interface';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient, private router: Router) {}

  user: User = null;
  userRoles: UserRole[] = [];

  logIn(user: User): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/login`, user);
  }

  logOut() {
    this.user = null;
    this.router.navigate(['login']);
  }

  registerUser(user: User): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/register`, user);
  }

  getUserRoles(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Roles`);
  }

  getCurrUserRole(): UserRole {
    if (this.user != null && this.userRoles.length !== 0) {
      const currentRole = this.userRoles.find(role => role.roleIndex === this.user.role);
      return currentRole;
    }
    return null;
  }

  getRoutes(): string[] {
    let routes: string[] = [];
    if (this.user != null && this.userRoles.length !== 0) {
      const currentRole = this.getCurrUserRole();
      switch (currentRole.roleName) {
        case 'SalesManager': {
          const temp = this.getRoutsForSalesManager();
          routes = temp;
          break;
        }
        case 'ProductManager': {
          const temp = this.getRoutsForProductManager();
          routes = temp;
          break;
        }
        case 'RawStaffManager': {
          const temp = this.getRoutsForRawStaffManager();
          routes = temp;
          break;
        }
        case 'Bookkeeper': {
          const temp = this.getRoutsForBookkeeper();
          routes = temp;
          break;
        }
        case 'Admin': {
          const temp = this.getRoutsForAdmin();
          routes = temp;
          break;
        }
      }
    } else {
      routes.push('login');
    }

    return routes;
  }

  getRoutsForProductManager(): string[] {
    const routs: string[] = [
      'products',
      'measures',
      'madedProducts',
      'removedProducts'
      ];

    return routs;
  }

  getRoutsForRawStaffManager(): string[] {
    const routs: string[] = [
      'rawStaff',
      'measures',
      'rawStaffOnStorage',
      'removedRawStaff',
      'rawStaffOrders',
      'suppliers'
      ];

    return routs;
  }

  getRoutsForBookkeeper(): string[] {
    const routs: string[] = [
      'outlay',
      'incomes',
      'employees'
      ];

    return routs;
  }

  getRoutsForSalesManager(): string[] {
    const routs: string[] = [
      'customers',
      'productOrders'
      ];

    return routs;
  }

  getRoutsForAdmin(): string[] {
    let temp = this.getRoutsForProductManager();
    const temp2 = this.getRoutsForSalesManager();
    const temp3 = this.getRoutsForBookkeeper();
    const temp4 = this.getRoutsForRawStaffManager();

    temp = temp.concat(temp2);
    temp = temp.concat(temp3);
    temp = temp.concat(temp4);

    temp.push('register');

    return temp;
  }
}
