import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import { RawStaff } from '../models/rawStaff.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RawStaffRequestService {
  private readonly apiUrl = environment.apiUrl;
  constructor(private httpClient: HttpClient) { }

  getAllRawStaff(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/RawStaff`);
  }

  setNewRawStaff(rawStaff: RawStaff): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/RawStaff`, rawStaff);
  }

  updateRawStaff(rawStaff: RawStaff): Observable<any> {
    return this.httpClient.put(`${this.apiUrl}/RawStaff`, rawStaff);
  }
  deleteRawStaff(id: number) {
    return this.httpClient.delete(`${this.apiUrl}/RawStaff/${id}`);
  }
}
