import { Injectable } from "@angular/core";
import { RawStaffOrder } from "../models/rawStaffOrder.interface";
import { RawStaffOrderItem } from "../models/rawStaffOrderItem.interface";
import { Subject } from "rxjs";
import { Supplier } from "../models/supplier.interface";
import { RawStaff } from "../models/rawStaff.interface";
import { Storage } from "../models/storage.interface";
import { OrderStatus } from "../models/orderStatus.interface";
import { Outlay } from "../models/outlay.interface";

@Injectable({
  providedIn: "root",
})
export class RawStaffOrdersService {
  constructor() {}

  public rawStaffOrdersList: RawStaffOrder[] = [];
  public orderItemsList: RawStaffOrderItem[] = [];
  public supplierSelect: Supplier[] = [];
  public rawStaffList: RawStaff[] = [];
  public storagesList: Storage[] = [];
  public abilityToUpdateOrder: boolean = null;
  public abilityToConfirmOrder: boolean = null;
  public abilityToCancelOrder: boolean = null;

  isOrderInProgress = true;
  orderToUpdate: RawStaffOrder = null;
  orderInsertedSubject = new Subject();
  onUpdateClickSubject = new Subject<RawStaffOrder>();

  getOrderStatus(statusesId: number[], order: RawStaffOrder) {
    if (statusesId.includes(order.idOrderStatus)) {
      this.isOrderInProgress = false;
    }
  }
}
