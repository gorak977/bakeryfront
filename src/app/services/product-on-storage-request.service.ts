import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../environments/environment';
import { Observable } from 'rxjs';
import { ProductOnStorage } from '../models/productOnStorage.interface';
import { IncertingProductOnStorage } from '../models/insertingProductOnStorage.interface';
import { RemovedProduct } from '../models/removedProduct.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductOnStorageRequestService {

  private readonly apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  getAllProductsOnStorage(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/MadeProduct`);
  }
  getProductAutocomplete(name: string): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/Product/GetByName/${name}`);
  }
  getAllStorages(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/MadeProduct/Storage`);
  }
  getNeededRawStaff(product: ProductOnStorage): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/MadeProduct/GetNeededRawStaff`, product);
  }
  setNewProduct(product: IncertingProductOnStorage): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/MadeProduct`, product);
  }
  removeProductFromStorage(product: RemovedProduct, productOnStorageId: number): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}/MadeProduct/RemoveProductFromStorage?productOnStorageId=${productOnStorageId}`, product);
  }
  getAllRemovedProduct(): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/MadeProduct/Removed/All`);
  }
}
