import { Injectable } from '@angular/core';
import { ProductsOrder } from '../models/productsOrder.interface';
import { ProductOrderItem } from '../models/productOrderItem.interface';
import { Subject } from 'rxjs';
import { Customer } from '../models/customer.interface';
import { Product } from '../models/product.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductOrdersService {

  constructor() { }

  public productOrdersList: ProductsOrder[] = [];
  public orderItemsList: ProductOrderItem[] = [];
  public customerSelect: Customer[] = [];
  public productAutocomplete: Product[] = [];

  isOrderInProgress = true;
  orderToUpdate: ProductsOrder = null;
  orderInsertedSubject = new Subject();
  onUpdateClickSubject = new Subject<ProductsOrder>();

  getOrderStatus(statusesId: number[], order: ProductsOrder) {
    if (statusesId.includes(order.idOrderStatus)) {
      this.isOrderInProgress = false;
    }
  }
}
